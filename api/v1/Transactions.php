<?php namespace Oppin\POS\API\V1;

use DB;
use Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use October\Rain\Extension\Extendable;
use Oppin\POS\Models\Transaction;
use Oppin\POS\Models\TransactionLine;
use Oppin\POS\Models\Payment;

class Transactions extends Extendable {
    public function latest(Request $request)
    {
        $transaction = $request->user()->transactions()->orderBy('created_at', 'desc')->first();
        if (!$transaction) {
            return response()->json([
                'authenticated' => true,
                'success'       => false,
                'message'       => 'Transaction not found',
            ], 404);
        }
        return response()->json([
            'authenticated' => true,
            'success'       => true,
            'result'        => $transaction->toArray(),
        ]);
    }

    public function post(Request $request)
    {
        foreach ($request->input('transactions') as $requestTransaction) {
            DB::transaction(function() use ($request, $requestTransaction) {
                $eod = null;

                // Save the transaction
                $transaction = $request->user()->transactions()->firstOrNew([
                    'client_id'  => $requestTransaction['local_id'],
                    'user_id'    => $requestTransaction['user_id'],
                    'currency'   => $requestTransaction['currency'],
                    'created_at' => Carbon::parse($requestTransaction['created_at']),
                ]);
                $transaction->status = $requestTransaction['status'];
                Event::fire('oppin.pos.api.v1.transactions.process', [$requestTransaction, &$transaction]);
                if ($transaction->isDirty()) {
                    $transaction->save();

                    // Save end of day details
                    if (!empty($requestTransaction['eod'])) {

                        $reqEod = $requestTransaction['eod'];
                        $eod = $transaction->end_of_day()->create();
                        foreach ($reqEod['expected'] as $id => $expected) {
                            $eod->payment_types()->attach($id, [
                                'start'    => !empty($reqEod['start'][$id]) ? $reqEod['start'][$id] : 0,
                                'expected' => $expected,
                                'actual'   => !empty($reqEod['actual'][$id]) ? $reqEod['actual'][$id] : 0,
                                'carry'    => !empty($reqEod['carry'][$id]) ? $reqEod['carry'][$id] : 0,
                            ]);
                        }
                    }
                }

                // If it's an EOD and already saved, don't re-add lines
                if (!empty($requestTransaction['eod']) && !$eod) {
                    return;
                }

                // Add the lines
                DB::beginTransaction();
                $lines = [];
                foreach ($requestTransaction['lines'] as $requestLine) {
                    $requestLine['created_at'] = Carbon::parse($requestLine['created_at']);
                    $filteredLine = array_filter($requestLine, function($key) {
                        return in_array($key, [
                            'name',
                            'short_name',
                            'qty',
                            'single_amount',
                            'line_amount',
                            'line_tax',
                            'created_at',
                        ]);
                    }, ARRAY_FILTER_USE_KEY);
                    $filteredLine['transaction_id'] = $transaction->id;
                    $filteredLine['tax_rate'] = !empty($requestLine['tax']) ? $requestLine['tax']['rate'] : null;
                    $filteredLine['single_amount'] = round($filteredLine['single_amount'], 2);
                    $filteredLine['line_amount'] = round($filteredLine['line_amount'], 2);
                    $filteredLine['line_tax'] = round($filteredLine['line_tax'], 2);

                    $line = empty($requestTransaction['eod']) ? TransactionLine::firstOrNew($filteredLine) : new TransactionLine($filteredLine);

                    // If transaction is for a product, store the product id
                    if (array_key_exists('product_id', $requestLine)) {
                        $line->product_id = $requestLine['product_id'];
                    }

                    // Handle payment lines, and set to zero for void transactions, to avoid messing up totals
                    if (array_key_exists('payment_id', $requestLine)) {
                        $line->payment_id = $requestLine['payment_id'];
                        if ($requestTransaction['status'] == 'void' && $line->object) {
                            $line->object->amount = 0;
                            $line->object->save();
                        }
                    }
                    if (array_key_exists('user_id', $requestLine)) {
                        $line->user_id = $requestLine['user_id'];
                    }
                    if (array_key_exists('eod_payment_id', $requestLine) && $requestLine['type'] != 'eod-label') {
                        $line->eod_payment_id = $requestLine['eod_payment_id'];
                    }
                    $line->type = $requestLine['type'];
                    Event::fire('oppin.pos.api.v1.transactions.processLine', [$requestTransaction, $requestLine, &$line]);
                    if (!$line->exists) {
                        $lines[] = $line;

                        // If change was given, adjust last cash payment accordingly
                        if ($requestLine['type'] == 'change' && $line->line_amount > 0) {
                            $paymentLines = collect($lines)
                                ->where('object_type', Payment::class)
                                ->reverse();
                            $paymentLines->each(function($pline, $key) use (&$lines, $line) {
                                if ($pline->object->payment_type->cash) {
                                    $lines[$key]->object->amount -= $line->line_amount;
                                    $lines[$key]->object->save();
                                    return false;
                                }
                            });
                        }
                    }
                }
                $transaction->lines()->addMany($lines);
                DB::commit();
            });
        }

        return response()->json([
            'authenticated' => true,
            'success'       => true
        ]);
    }
}
