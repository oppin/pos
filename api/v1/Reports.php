<?php namespace Oppin\POS\API\V1;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use October\Rain\Extension\Extendable;
use Oppin\POS\Models\TransactionLine;
use Oppin\POS\Models\Payment;

class Reports extends Extendable {
    public function totals(Request $request, $date = null)
    {
        if ($date == null || !strtotime($date)) {
            $date = Carbon::now()->startOfDay();
        } else {
            $date = Carbon::parse($date)->startOfDay();
        }

        // TODO - Category/tag totals

        $totals = TransactionLine::whereHas('transaction', function($query) use ($date, $request) {
                $query->where('status', 'complete')
                    ->whereDate('created_at', $date->format('Y-m-d'))
                    ->whereHas('terminal', function($query) use ($request) {
                        $query->where('location_id', $request->user()->location_id);
                    });
            })
            ->where('object_type', 'Oppin\POS\Models\Product')
            ->addSelect(DB::raw(
                'COUNT(DISTINCT transaction_id) AS transaction_count, ' .
                'COUNT(DISTINCT CASE WHEN line_amount > 0 THEN transaction_id ELSE NULL END) AS income_count, ' .
                'COUNT(DISTINCT CASE WHEN line_amount < 0 THEN transaction_id ELSE NULL END) AS refund_count, ' .
                'SUM(CASE WHEN line_amount > 0 THEN line_amount ELSE 0 END) AS income_total_amount, ' .
                'SUM(CASE WHEN line_tax > 0 THEN line_tax ELSE 0 END) AS income_total_tax, ' .
                'SUM(CASE WHEN line_amount < 0 THEN line_amount * -1 ELSE 0 END) AS refund_total_amount, ' .
                'SUM(CASE WHEN line_tax < 0 THEN line_tax * -1 ELSE 0 END) AS refund_total_tax, ' .
                'SUM(line_amount) AS total_amount, ' .
                'SUM(line_tax) AS total_tax'
            ))
            ->first()
            ->toArray();
        $totals = array_map(function($total) {
            // TODO: Needs improving to handle multiple currency formats
            return round($total, 2);
        }, $totals);

        $payments = Payment::whereHas('transaction_line', function($query) use ($date, $request) {
                $query->whereHas('transaction', function($query) use ($date, $request) {
                    $query->where('status', 'complete')
                        ->whereDate('created_at', $date->format('Y-m-d'))
                        ->whereHas('terminal', function($query) use ($request) {
                            $query->where('location_id', $request->user()->location_id);
                        });
                });
            })
            ->join('oppin_pos_payment_types AS payment_types', 'oppin_pos_payments.payment_type_id', '=', 'payment_types.id')
            ->join('oppin_pos_transaction_lines AS transaction_lines', function($join) {
                $join->on('oppin_pos_payments.id', '=', 'transaction_lines.object_id')
                    ->where('transaction_lines.object_type', Payment::class);
            })
            ->groupBy('payment_type_id')
            ->addSelect(DB::raw(
                'payment_types.name, ' .
                'payment_types.short_name, ' .
                'COUNT(DISTINCT transaction_lines.transaction_id) AS transaction_count, ' .
                'SUM(CASE WHEN amount > 0 THEN amount ELSE 0 END) AS income_total_amount, ' .
                'SUM(CASE WHEN amount < 0 THEN amount * -1 ELSE 0 END) AS refund_total_amount, ' .
                'SUM(amount) AS total_amount'
            ))
            ->get()
            ->toArray();

        return response()->json([
            'authenticated' => true,
            'success'       => true,
            'result'        => compact('totals', 'payments'),
        ]);
    }
}
