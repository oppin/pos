<?php namespace Oppin\POS\API\V1;

use Event;
use Carbon\Carbon;
use CommerceGuys\Tax\Repository\TaxTypeRepository;
use Illuminate\Http\Request;
use October\Rain\Extension\Extendable;
use Oppin\POS\Models\Product;
use Oppin\POS\Models\Barcode;
use Oppin\POS\Models\ProductPrice;
use Oppin\POS\Models\Settings;
use Oppin\POS\Models\PaymentType;

class Terminal extends Extendable {

    public function update(Request $request)
    {
        $terminal = $request->user();
        $terminal->last_seen = Carbon::now();
        $terminal->save();

        // TODO: Don't include user barcodes that cannot log in in this location
        $barcodes = Barcode::all();

        $products = Product::active()
            ->with(['children', 'prices' => function($query) {
                $query->select('product_id', 'min_qty', 'currency', 'sale_price', 'tax_rate');
            }])
            ->get()
            ->keyBy('id');

        $buttonConfig = collect($terminal->location->buttons);

        $buttons = [];
        $counter = 1;
        foreach ($buttonConfig as $b) {
            if ($b['_group'] == 'product' && $product = $products->get($b['product_id'])) {
                $button = [
                    'id'    => $counter,
                    'label' => $product['name'],
                    'type'  => 'product',
                ];
                $counter++;
                if (!$product['children']->count()) {
                    $button['product_id'] = $b['product_id'];
                } else {
                    $buttonChildren = [];
                    foreach ($product['children'] as $pc) {
                        $buttonChildren[] = [
                            'id'         => $counter,
                            'label'      => $pc['name'],
                            'type'       => 'product',
                            'product_id' => $pc['id'],
                        ];
                        $counter++;
                    }
                    $button['buttons'] = $buttonChildren;
                }
                $buttons[] = $button;
            } else {
                $buttons[] = [
                    'id'    => $counter,
                    'label' => $b['_group'],
                    'type'  => $b['_group']
                ];
                $counter++;
            }
        }

        $repo = new TaxTypeRepository();
        $types = $repo->getAll();
        $rates = [];
        foreach ($types as $type) {
            $rates[$type->getId() . '_exempt'] = [
                'rate'    => $type->getId() . '_exempt',
                'label'   => $type->getName() . ' Exempt',
                'percent' => 0,
            ];
            $typeRates = $type->getRates();
            foreach ($typeRates as $rate) {
                foreach ($rate->getAmounts() as $amount) {
                    if (is_null($amount->getEndDate())) {
                        $rates[$rate->getId()] = [
                            'rate'    => $rate->getId(),
                            'label'   => $type->getName() . ' ' . $rate->getName(),
                            'percent' => $amount->getAmount() * 100,
                        ];
                    }
                }
            }
        }
        $enabledRates = Settings::get('tax_rates', []);
        $taxRates = [];
        foreach($enabledRates as $rate) {
            $taxRates[$rate] = $rates[$rate];
        }

        $users = $terminal->location->users()
            ->select('id', 'first_name', 'login')
            ->active()
            ->with(['roles' => function($query) {
                $query->select('id', 'name', 'permissions');
            }])
            ->orderBy('id', 'asc')
            ->get()
            ->keyBy('id');

        $paymentTypes = $terminal->payment_types()
            ->active()
            ->get()
            ->keyBy('id');

        $result = [
            'barcodes'      => $barcodes,
            'buttons'       => $buttons,
            'payment_types' => $paymentTypes,
            'products'      => $products,
            'receipt'       => [
                'header' => explode("\n", $terminal->location->receipt_header),
                'footer' => explode("\n", $terminal->location->receipt_footer),
            ],
            'tax_rates'     => $taxRates,
            'terminal'      => ['name' => $terminal->name],
            'users'         => $users,
        ];

        Event::fire('oppin.pos.api.v1.terminal.update', [&$result, $terminal]);

        return response()->json([
            'authenticated' => true,
            'success'       => true,
            'result'        => $result,
        ]);
    }
}
