<?php namespace Oppin\POS\Models;

use Model;
use CommerceGuys\Tax\Repository\TaxTypeRepository;

/**
 * ProductPrice Model
 */
class ProductPrice extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_product_prices';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'min_qty',
        'currency',
        'sale_price',
        'tax_rate',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'min_qty'    => 'required|min:0.01',
        'currency'   => 'sometimes|required|currency_code',
        'sale_price' => 'required|numeric|min:0',
        'tax_rate'   => 'required|tax_rate',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'product' => 'Oppin\POS\Models\Product',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    //
    // Dropdown Options
    //

    public function getTaxRateOptions()
    {
        $repo = new TaxTypeRepository();
        $types = $repo->getAll();
        $rates = [];
        foreach ($types as $type) {
            $rates[$type->getId() . '_exempt'] = [
                $type->getName() . ' Exempt',
                'Exempt from tax'
            ];
            $typeRates = $type->getRates();
            foreach ($typeRates as $rate) {
                foreach ($rate->getAmounts() as $amount) {
                    if (is_null($amount->getEndDate())) {
                        $rates[$rate->getId()] = [
                            $type->getName() . ' ' . $rate->getName(),
                            'Currently ' . ($amount->getAmount() * 100) . '%'
                        ];
                    }
                }
            }
        }
        $enabledRates = Settings::get('tax_rates', []);
        $result = [];
        foreach($enabledRates as $rate) {
            $result[$rate] = $rates[$rate];
        }
        return $result;
    }
}
