<?php namespace Oppin\POS\Models;

use Model;
use Carbon\Carbon;

/**
 * User Model
 */
class User extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'first_name',
        'last_name',
    ];

    /**
     * @var array Fields to convert to Carbon objects
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'first_name' => 'required|alpha|max:20',
        'last_name'  => 'required|alpha|max:30',
        'login'      => 'sometimes|required|string|size:5',
        'is_active'  => 'boolean',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'transactions' => 'Oppin\POS\Models\Transaction',
    ];
    public $belongsTo = [];
    public $belongsToMany = [
        'locations'    => 'Oppin\POS\Models\Location',
        'roles'        => 'Oppin\POS\Models\Role',
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [
        'barcodes' => [
            'Oppin\POS\Models\Barcode',
            'name' => 'object'
        ],
    ];
    public $attachOne = [];
    public $attachMany = [];

    /*
     | Events
    */

    public function beforeCreate()
    {
        do {
            $this->login = str_pad(rand(0, 99999), 5, '0', STR_PAD_LEFT);
        } while (User::where('login', $this->login)->count());
    }

    /**
     * Dynamically include record's own id into validation rules
     */
    public function beforeValidate()
    {
        $this->rules['login'] .= '|unique:oppin_pos_users,login,' . $this->id;
    }

    /*
     | Dynamic attributes
    */
    public function getLastSeenAttribute()
    {
        $seen = $this->transactions()->orderBy('created_at', 'desc')->value('created_at');
        return $seen ? Carbon::parse($seen) : null;
    }

    public function getBarcodesTextAttribute()
    {
        return $this->barcodes->implode('barcode', "\n");
    }

    public function setBarcodesTextAttribute($value)
    {
        $barcodes = collect(explode("\n", str_replace("\r\n", "\n", $value)));
        $barcodes = $barcodes->map(function ($barcode) {
            return trim($barcode);
        })->filter(function($barcode) {
            return !empty($barcode);
        });

        // Check if barcode exists on this product
        $existing = collect($this->barcodes()->withDeferred(post('_session_key'))->lists('barcode'));

        // Find which barcodes need creating or deleting
        $new = $barcodes->diff($existing);
        $removed = $existing->diff($barcodes);

        // Create new barcodes
        foreach ($new as $barcode) {
            $model = Barcode::create([
                'barcode' => $barcode
            ]);
            $this->barcodes()->add($model, post('_session_key'));
        }

        // Delete old barcodes
        foreach ($removed as $barcode) {
            $model = $this->barcodes()
                ->withDeferred(post('_session_key'))
                ->where('barcode', $barcode)
                ->first();
            $this->barcodes()->delete($model, post('_session_key'));
        }
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /*
     | Scopes
    */
    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }
}
