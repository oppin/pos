<?php namespace Oppin\POS\Models;

use Model;

/**
 * Role Model
 */
class Role extends Model
{
    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_roles';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'permissions',
    ];

    /**
     * @var array Fields to convert to/from JSON
     */
    protected $jsonable = ['permissions'];

    /**
     * @var array Nullable attributes.
     */
    protected $nullable = ['permissions'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'            => 'required|max:50',
        'permissions'     => 'nullable|array',
        'is_system'       => 'boolean'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'users' => 'Oppin\POS\Models\User',
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
