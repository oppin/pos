<?php namespace Oppin\POS\Models;

use Model;

/**
 * PaymentType Model
 */
class PaymentType extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_payment_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'currency',
        'is_active',
        'cash',
        'show_other',
        'amounts'
    ];

    /**
     * @var array Fields to convert to Carbon objects
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array Attribute names to encode and decode using JSON.
     */
    protected $jsonable = ['amounts'];

    /**
     * @var array The attributes to hide from the model's array form.
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'pivot',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'       => 'required|max:30',
        'currency'   => 'required|currency_code',
        'is_active'  => 'boolean',
        'cash'       => 'boolean',
        'show_other' => 'boolean',
        'amounts'    => 'nullable|array',
        'amounts.*'  => 'numeric',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'payments'    => 'Oppin\POS\Models\Payment',
    ];
    public $belongsTo = [];
    public $belongsToMany = [
        'end_of_days' => [
            'Oppin\POS\Models\EndOfDay',
            'pivot' => ['start', 'expected', 'actual', 'carry']
        ],
        'terminals'   => 'Oppin\POS\Models\Terminal',
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    //
    // Scopes
    //

    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }
}
