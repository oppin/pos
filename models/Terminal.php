<?php namespace Oppin\POS\Models;

use Model;

/**
 * Terminal Model
 */
class Terminal extends Model
{
    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_terminals';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'currency',
        'status',
        'last_seen',
    ];

    /**
     * @var bool Indicates if the model should be timestamped automatically.
     */
    public $timestamps = false;

    /**
     * @var array Fields to convert to Carbon objects
     */
    protected $dates = [
        'last_seen',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var array Nullable attributes.
     */
    protected $nullable = ['last_seen'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'api_key'         => 'required|alpha_num|size:30',
        'name'            => 'required|max:50',
        'currency'        => 'sometimes|required|currency_code',
        'is_active'       => 'boolean',
        'status'          => 'sometimes|required|in:unknown,closed,signed_off,single_sign_on,signed_on',
        'last_seen'       => 'nullable|date'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'transactions'  => 'Oppin\POS\Models\Transaction',
    ];
    public $belongsTo = [
        'location'      => 'Oppin\POS\Models\Location',
        'transaction'   => 'Oppin\POS\Models\Transaction', // Current transaction
        'user'          => 'Oppin\POS\Models\User',        // Current user
    ];
    public $belongsToMany = [
        'payment_types' => 'Oppin\POS\Models\PaymentType',
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function __construct(array $attributes = [])
    {
        do {
            $this->api_key = str_random(30);
        } while (Terminal::where('api_key', $this->api_key)->count());
        parent::__construct($attributes);
    }

    /*
     | Custom attributes
    */
    public function getStatusNameAttribute()
    {
        return ucwords(str_replace('_', ' ', $this->status));
    }

    /*
     | Events
    */
    public function beforeCreate()
    {
        do {
            $this->api_key = str_random(30);
        } while (Terminal::where('api_key', $this->api_key)->count());
    }
}
