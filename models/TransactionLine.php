<?php namespace Oppin\POS\Models;

use Model;
use Oppin\POS\Classes\Action;
use Oppin\POS\Classes\Change;
use Oppin\POS\Classes\Eod;
use Oppin\POS\Classes\EodCarry;
use Oppin\POS\Classes\EodLabel;
use Oppin\POS\Classes\EodPayment;
use Oppin\POS\Classes\Tax;
use Oppin\POS\Classes\Total;
use Oppin\POS\Classes\VoidTransaction;

/**
 * TransactionLine Model
 */
class TransactionLine extends Model
{
    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_transaction_lines';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'short_name',
        'tax_rate',
        'qty',
        'product_id',
        'single_amount',
        'line_amount',
        'line_tax',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array Nullable attributes.
     */
    protected $nullable = [
        'name',
        'short_name',
        'tax_rate',
        'qty',
        'single_amount',
        'line_amount',
        'line_tax',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'          => 'nullable|max:255',
        'short_name'    => 'nullable|max:20',
        'tax_rate'      => 'nullable|tax_rate',
        'qty'           => 'nullable|numeric|required_with:single_amount,single_tax,line_amount,line_tax',
        'single_amount' => 'nullable|numeric|required_with:single_tax,line_amount,line_tax',
        'line_amount'   => 'nullable|numeric|required_with:single_amount,single_tax,line_tax',
        'line_tax'      => 'nullable|numeric|required_with:single_amount,single_tax,line_amount',
        'created_at'    => 'date',
        'updated_at'    => 'date',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'transaction' => 'Oppin\POS\Models\Transaction',
    ];
    public $belongsToMany = [];
    public $morphTo = [
        'object' => [],
    ];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function setEodPaymentIdAttribute($value)
    {
        $this->attributes['object_id'] = $value;
    }

    /**
     * Payment ID passed by frontend = PaymentType ID in backend
     * This method creates a new payment record linked to the payment type
     * referenced, and links this transaction line to the new record.
     *
     * @param int $value
     * @return void
     */
    public function setPaymentIdAttribute($value)
    {
        if (!$this->exists) {
            $paymentType = PaymentType::find($value);
            if ($paymentType) {
                $payment = $paymentType->payments()->create([
                    'amount' => $this->line_amount * -1,
                ]);
                $this->attributes['object_id'] = $payment->id;
            }
            $this->attributes['object_type'] = Payment::class;
        }
    }

    public function setProductIdAttribute($value)
    {
        $this->attributes['object_id'] = $value;
        $this->attributes['object_type'] = Product::class;
    }

    public function setUserIdAttribute($value)
    {
        $this->attributes['object_id'] = $value;
        $this->attributes['object_type'] = User::class;
    }

    public function setTypeAttribute($value)
    {
        $typeMap = [
            'action'      => Action::class,
            'change'      => Change::class,
            'eod'         => Eod::class,
            'eod-carry'   => EodCarry::class,
            'eod-label'   => EodLabel::class,
            'eod-payment' => EodPayment::class,
            'tax'         => Tax::class,
            'total'       => Total::class,
            'void'        => VoidTransaction::class,
        ];
        if (array_key_exists($value, $typeMap)) {
            $this->attributes['object_type'] = $typeMap[$value];
        }
    }

    //
    // Scopes
    //

    public function scopeLocations($query, $locations)
    {
        $query->whereHas('transaction.terminal', function($query) use ($locations) {
            $query->whereIn('location_id', $locations);
        });
    }

    public function scopeTerminals($query, $terminals)
    {
        $query->whereHas('transaction', function($query) use ($terminals) {
            $query->whereIn('terminal_id', $terminals);
        });
    }
}
