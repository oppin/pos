<?php namespace Oppin\POS\Models;

use Model;

/**
 * Payment Model
 */
class Payment extends Model
{
    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_payments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'amount',
        'meta'
    ];

    /**
     * @var array Fields to convert to/from JSON
     */
    protected $jsonable = ['meta'];

    /**
     * @var array Nullable attributes.
     */
    protected $nullable = ['meta'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'amount' => 'required|numeric',
        'meta'   => 'nullable|array',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'payment_type' => 'Oppin\POS\Models\PaymentType',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [
        'transaction_line' => [
            'Oppin\POS\Models\TransactionLine',
            'name' => 'object'
        ]
    ];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    //
    // Scopes
    //

    public function scopeLocations($query, $locations)
    {
        $query->whereHas('transaction_line.transaction.terminal', function($query) use ($locations) {
            $query->whereIn('location_id', $locations);
        });
    }

    public function scopeTerminals($query, $terminals)
    {
        $query->whereHas('transaction_line.transaction', function($query) use ($terminals) {
            $query->whereIn('terminal_id', $terminals);
        });
    }
}
