<?php namespace Oppin\POS\Models;

use Model;

/**
 * EndOfDay Model
 */
class EndOfDay extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_end_of_days';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'transaction'   => 'Oppin\POS\Models\Transaction',
    ];
    public $belongsToMany = [
        'payment_types' => [
            'Oppin\POS\Models\PaymentType',
            'pivot' => ['start', 'expected', 'actual', 'carry']
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    //
    // Scopes
    //

    public function scopeLocations($query, $locations)
    {
        $query->whereHas('transaction.terminal', function($query) use ($locations) {
            $query->whereIn('location_id', $locations);
        });
    }
}
