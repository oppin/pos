<?php namespace Oppin\POS\Models;

use Model;
use Carbon\Carbon;
use Oppin\POS\Models\Barcode;
use CommerceGuys\Tax\Repository\TaxTypeRepository;
use October\Rain\Exception\ValidationException;

/**
 * Product Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Purgeable;
    use \October\Rain\Database\Traits\SimpleTree;
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'short_name',
        'sku',
        'cost_price',
        'cost_price_per',
        'price_per_unit'
    ];

    /**
     * @var array Fields to convert to Carbon objects
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array Attribute names to encode and decode using JSON.
     */
    protected $jsonable = ['meta'];

    /**
     * @var array Nullable attributes.
     */
    protected $nullable = [
        'short_name',
        'sku',
        'cost_price',
        'cost_price_per',
        'price_per_unit',
        'meta',
    ];

    /**
     * @var array List of attributes to purge.
     */
    protected $purgeable = [
        'type',
        'pricing_type',
        'sale_price',
        'tax_rate',
    ];

    /**
     * @var array The attributes to hide from the model's array form.
     */
    protected $hidden = [
        'status_id',
        'cost_price',
        'cost_price_currency',
        'cost_price_per',
        'meta',
        'created_at',
        'deleted_at',
    ];

    /**
     * @var array The accessors to append to the model's array form.
     */
    public $appends = [
        'type',
        'pricing_type',
        'sale_price',
        'tax_rate',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'                    => 'required|max:255',
        'short_name'              => 'nullable|max:20',
        'type'                    => 'required_without:parent_id|in:simple,variations,variant',
        'sku'                     => 'nullable|max:30',
        'cost_price'              => 'nullable|numeric|min:0',
        'cost_price_per'          => 'nullable|required_with:cost_price|numeric|min:0.01',
        'price_per_unit'          => 'nullable|alpha|max:10',
        'pricing_type'            => 'required_without:parent_id|in:inherit,simple,quantity',
        'sale_price'              => 'nullable|required_if:pricing_type,simple|numeric|min:0',
        'tax_rate'                => 'nullable|required_if:pricing_type,simple|tax_rate',
        'cost_price_type'         => 'sometimes|required|in:inherit,variant',
        'meta.*.name'             => 'required_with:meta.*.value',
        'meta.*.value'            => 'required_with:meta.*.name',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'prices'   => 'Oppin\POS\Models\ProductPrice',
    ];
    public $belongsTo = [
        'status'   => [
            'Oppin\POS\Models\ProductStatus',
            'key' => 'status_id',
        ]
    ];
    public $belongsToMany = [
        'tags'     => [
            'Oppin\POS\Models\Tag',
            'order' => 'name asc',
            'pivot' => ['primary'],
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [
        'barcodes' => [
            'Oppin\POS\Models\Barcode',
            'name' => 'object'
        ],
        'transaction_lines' => [
            'Oppin\POS\Models\TransactionLine',
            'name' => 'object'
        ]
    ];
    public $attachOne = [];
    public $attachMany = [];

    //
    // Mutators
    //
    public function getTypeAttribute()
    {
        if ($this->children()->count()) {
            return 'variations';
        } elseif (!empty($this->parent_id)) {
            return 'variant';
        }
        return 'simple';
    }

    public function getBarcodesTextAttribute()
    {
        return $this->barcodes->implode('barcode', "\n");
    }

    public function setBarcodesTextAttribute($value)
    {
        $barcodes = collect(explode("\n", str_replace("\r\n", "\n", $value)));
        $barcodes = $barcodes->map(function ($barcode) {
            return trim($barcode);
        })->filter(function($barcode) {
            return !empty($barcode);
        });

        // Check if barcode exists on this product
        $existing = collect($this->barcodes()->withDeferred(post('_session_key'))->lists('barcode'));

        // Find which barcodes need creating or deleting
        $new = $barcodes->diff($existing);
        $removed = $existing->diff($barcodes);

        // Create new barcodes
        foreach ($new as $barcode) {
            $model = Barcode::create([
                'barcode' => $barcode
            ]);
            $this->barcodes()->add($model, post('_session_key'));
        }

        // Delete old barcodes
        foreach ($removed as $barcode) {
            $model = $this->barcodes()
                ->withDeferred(post('_session_key'))
                ->where('barcode', $barcode)
                ->first();
            $this->barcodes()->delete($model, post('_session_key'));
        }
    }

    public function getPrimaryTagAttribute()
    {
        return $this->tags()->wherePivot('primary', 1)->value('id');
    }

    public function setPrimaryTagAttribute($value)
    {
        $current = $this->tags()->wherePivot('primary', 1)->get();
        foreach($current as $tag) {
            $tag->pivot->primary = 0;
            $tag->save();
        }
        $this->tags()->syncWithoutDetaching($value, ['primary' => 1,]);
    }

    public function getPricingTypeAttribute()
    {
        if ($this->prices->count() > 1) {
            return 'quantity';
        } elseif ($this->prices->count() == 1) {
            return 'simple';
        }
        return $this->exists ? 'inherit' : null;
    }

    public function getPricePerUnitDropdownAttribute()
    {
        if (empty($this->price_per_unit)) {
            return 'ea';
        }
        return in_array($this->price_per_unit, [
            'ea', 'g', 'kg', 'ml', 'l', 'cm', 'm'
        ]) ? $this->price_per_unit : 'other';
    }

    public function setPricePerUnitDropdownAttribute($value)
    {
        if ($value != 'other') {
            $this->price_per_unit = $value;
        }
    }

    public function getSalePriceAttribute()
    {
        return $this->prices->count() >= 1 || !$this->parent_id
            ? $this->prices()->where('min_qty', 1)->value('sale_price')
            : $this->parent->prices()->where('min_qty', 1)->value('sale_price');
    }

    public function getTaxRateAttribute()
    {
        return $this->prices->count() >= 1 || !$this->parent_id
            ? $this->prices()->where('min_qty', 1)->value('tax_rate')
            : $this->parent->prices()->where('min_qty', 1)->value('tax_rate');
    }

    public function getCostPriceTypeAttribute()
    {
        if (empty($this->cost_price) && empty($this->cost_price_per)) {
            return 'inherit';
        } else {
            return 'variant';
        }
    }

    public function setCostPriceTypeAttribute($value)
    {
        if ($value == 'inherit') {
            $this->cost_price = null;
            $this->cost_price_per = null;
        }
    }

    //
    // Dropdown Options
    //

    public function getPrimaryTagOptions()
    {
        return $this->tags()->lists('name', 'id');
    }

    public function getTaxRateOptions()
    {
        $repo = new TaxTypeRepository();
        $types = $repo->getAll();
        $rates = [];
        foreach ($types as $type) {
            $rates[$type->getId() . '_exempt'] = [
                $type->getName() . ' Exempt',
                'Exempt from tax'
            ];
            $typeRates = $type->getRates();
            foreach ($typeRates as $rate) {
                foreach ($rate->getAmounts() as $amount) {
                    if (is_null($amount->getEndDate())) {
                        $rates[$rate->getId()] = [
                            $type->getName() . ' ' . $rate->getName(),
                            'Currently ' . ($amount->getAmount() * 100) . '%'
                        ];
                    }
                }
            }
        }
        $enabledRates = Settings::get('tax_rates', []);
        $result = [];
        foreach($enabledRates as $rate) {
            $result[$rate] = $rates[$rate];
        }
        return $result;
    }

    //
    // Events
    //

    public function beforeSave()
    {
        // If the pricing set to quantity
        if ($this->getOriginalPurgeValue('pricing_type') == 'quantity') {
            // Validate that at least one price exists
            if (!$this->prices()->withDeferred(post('_session_key'))->exists()) {
                throw new ValidationException(['prices' => 'A product must have at least one price set, but it can be zero.']);
            }
        }
    }

    public function afterSave()
    {
        // If type set to simple, delete variations
        if ($this->getOriginalPurgeValue('type') == 'simple') {
            $this->children()->update([
                'deleted_at' => Carbon::now()
            ]);
        }
        // If pricing set to inherit
        if ($this->getOriginalPurgeValue('pricing_type') == 'inherit') {
            // Delete price rules
            $prices = $this->prices()->delete();
        // If the pricing set to simple
        } elseif ($this->getOriginalPurgeValue('pricing_type') == 'simple') {
            // Delete all non-single qty prices
            $prices = $this->prices()->where('min_qty', '<>', 1)->delete();
            // Create/update single qty price
            $this->prices()->firstOrNew([
                'min_qty' => 1
            ])->fill([
                'sale_price' => $this->getOriginalPurgeValue('sale_price'),
                'tax_rate'   => $this->getOriginalPurgeValue('tax_rate'),
            ])->forceSave();
        }
    }

    public function beforeDelete()
    {
        $this->children()->update([
            'deleted_at' => Carbon::now()
        ]);
    }

    //
    // Scopes
    //

    public function scopeActive($query)
    {
        $query->whereHas('status', function($query) {
            $query->where('is_active', true);
        });
    }

}
