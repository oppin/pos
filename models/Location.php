<?php namespace Oppin\POS\Models;

use Model;

/**
 * Location Model
 */
class Location extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_locations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name'];

    /**
     * @var array Fields to convert to Carbon objects
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array Fields to convert to JSON for storage
     */
    protected $jsonable = ['buttons'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|max:255',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'terminals' => 'Oppin\POS\Models\Terminal',
    ];
    public $belongsTo = [];
    public $belongsToMany = [
        'users'     => 'Oppin\POS\Models\User',
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    //
    // Dropdown Options
    //

    public function getProductIdOptions()
    {
        return Product::whereNull('deleted_at')->listsNested('name', 'id');
    }
}
