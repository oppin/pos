<?php namespace Oppin\POS\Models;

use Event;
use Model;
use Carbon\Carbon;
use CommerceGuys\Tax\Repository\TaxTypeRepository;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'oppin_pos_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

    //
    // Mutators
    //

    public function getProductStatusesAttribute()
    {
        return ProductStatus::select('name', 'is_active')
            ->orderBy('name')
            ->get()
            ->toArray();
    }

    public function setProductStatusesAttribute($value)
    {
        ProductStatus::whereNotIn('name', collect($value)->pluck('name'))
            ->delete();
        foreach ($value as $status) {
            $record = ProductStatus::firstOrNew([
                'name' => $status['name']
            ]);
            $record->is_active = !empty($status['is_active']);
            $record->save();
        }
    }

    public function getPaymentTypesAttribute()
    {
        return PaymentType::all()->toArray();
    }

    public function setPaymentTypesAttribute($value)
    {
        PaymentType::whereNotIn('id', collect($value)->pluck('id'))
            ->update([
                'deleted_at' => Carbon::now()
            ]);
        foreach ($value as $payment_type) {
            if (!empty($payment_type['id'])) {
                $record = PaymentType::find($payment_type['id']);
            } else {
                $record = new PaymentType;
            }
            $record->currency   = 'GBP';
            $record->name       = $payment_type['name'];
            $record->short_name = $payment_type['short_name'];
            $record->is_active  = !empty($payment_type['is_active']);
            $record->cash       = !empty($payment_type['cash']);
            $record->show_other = !empty($payment_type['show_other']);
            $record->amounts    = !empty($payment_type['amounts']) ? $payment_type['amounts'] : null;

            // Extensibility
            Event::fire('oppin.pos.settings.paymentTypes.beforeSave', [&$record, $payment_type]);

            $record->save();
        }
    }

    //
    // List options
    //

    public function getTaxRatesOptions()
    {
        $repo = new TaxTypeRepository();
        $types = $repo->getAll();
        $result = [];
        foreach ($types as $type) {
            $result[$type->getId() . '_exempt'] = [
                $type->getName() . ' Exempt',
                'Exempt from tax'
            ];
            $rates = $type->getRates();
            foreach ($rates as $rate) {
                foreach ($rate->getAmounts() as $amount) {
                    if (is_null($amount->getEndDate())) {
                        $result[$rate->getId()] = [
                            $type->getName() . ' ' . $rate->getName(),
                            'Currently ' . ($amount->getAmount() * 100) . '%'
                        ];
                    }
                }
            }
        }
        uasort($result, function($a, $b) {
            return strcmp($a[0], $b[0]);
        });
        return $result;
    }
}
