<?php namespace Oppin\POS\Models;

use Model;

/**
 * Barcode Model
 */
class Barcode extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_barcodes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['barcode'];

    /**
     * @var array Fields to convert to Carbon objects
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array The accessors to append to the model's array form.
     */
    protected $appends = [
        'type',
    ];

    /**
     * @var array The attributes to hide from the model's array form.
     */
    protected $hidden = [
        'id',
        'object_type',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'barcode' => 'required|max:30',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [
        'object' => [],
    ];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array Class to short name dictionary
     */
    public $typeMap = [
        'Oppin\POS\Models\Product' => 'product',
        'Oppin\POS\Models\User'    => 'user',
    ];

    //
    // Mutators
    //

    public function getTypeAttribute()
    {
        return $this->typeMap[$this->object_type];
    }
}
