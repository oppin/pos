<?php namespace Oppin\POS\Models;

use Model;

/**
 * Transaction Model
 */
class Transaction extends Model
{
    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\SimpleTree;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_transactions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'client_id',
        'parent_id',
        'terminal_id',
        'user_id',
        'currency',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Nullable attributes.
     */
    protected $nullable = [
        'parent_id',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'client_id'       => 'nullable|integer|min:1',
        'parent_id'       => 'nullable|integer|exists:oppin_pos_transactions,id',
        'terminal_id'     => 'required|integer|exists:oppin_pos_terminals,id,is_active,1',
        'user_id'         => 'required|integer|exists:oppin_pos_users,id,is_active,1',
        'currency'        => 'required|currency_code',
        'status'          => 'required|in:open,complete,suspended,void',
        'created_at'      => 'date',
        'updated_at'      => 'date',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'end_of_day' => 'Oppin\POS\Models\EndOfDay',
    ];
    public $hasMany = [
        'lines'      => 'Oppin\POS\Models\TransactionLine',
    ];
    public $belongsTo = [
        'terminal'   => 'Oppin\POS\Models\Terminal',
        'user'       => 'Oppin\POS\Models\User',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /*
     | Events
    */

    /**
     * Dynamically include record's own id into validation rules
     */
    public function beforeValidate()
    {
        $this->rules['parent_id'] .= '|unique:oppin_pos_transactions,parent_id,' . $this->id;
    }

    /*
     | Scopes
    */
    public function scopeLocations($query, $location)
    {
        $query->whereHas('terminal', function($query) use ($location) {
            $query->where('location_id', $location);
        });
    }
}
