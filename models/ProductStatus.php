<?php namespace Oppin\POS\Models;

use Model;

/**
 * ProductStatus Model
 */
class ProductStatus extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_pos_product_statuses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'is_active',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'      => 'required|max:50',
        'is_active' => 'boolean',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => 'Oppin\POS\Models\Product',
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
