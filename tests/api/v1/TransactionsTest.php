<?php namespace Oppin\POS\Tests\API\V1;

use PluginTestCase;
use Faker\Factory;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Oppin\POS\Models\Terminal;
use System\Classes\PluginManager;
use Carbon\Carbon;
use Oppin\POS\Models\Product;
use Oppin\POS\Classes\Total;
use Oppin\POS\Classes\Tax;
use Oppin\POS\Models\Payment;
use Oppin\POS\Classes\Change;
use Oppin\POS\Classes\VoidTransaction;
use Oppin\POS\Classes\Action;
use Oppin\POS\Models\User;
use Oppin\POS\Classes\Eod;
use Oppin\POS\Classes\EodLabel;
use Oppin\POS\Classes\EodPayment;
use Oppin\POS\Classes\EodCarry;
use Oppin\POS\Models\TransactionLine;
use Oppin\POS\Models\Transaction;

/**
 * @coversDefaultClass \Oppin\POS\API\V1\Transactions
 */
class TransactionsTest extends PluginTestCase
{
    public function setUp()
    {
        parent::setUp();

        // Get the plugin manager
        $pluginManager = PluginManager::instance();

        // Register the plugins to make features like file configuration available
        $pluginManager->registerAll(true);

        // Boot all the plugins to test with dependencies of this plugin
        $pluginManager->bootAll(true);
    }

    public function tearDown()
    {
        parent::tearDown();

        // Get the plugin manager
        $pluginManager = PluginManager::instance();

        // Ensure that plugins are registered again for the next test
        $pluginManager->unregisterAll();
    }

    /**
     * @covers ::latest
     */
    public function testLatest()
    {
        $api = Terminal::first()->api_key;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $api
        ])
        ->json('GET', '/api/v1/transactions/latest');

        $response->assertJson([
            'authenticated' => true,
            'success'       => false,
            'message'       => 'Transaction not found',
        ]);

        Transaction::create([
            'terminal_id' => 1,
            'user_id'     => 1,
            'currency'    => 'GBP',
            'status'      => 'complete',
        ]);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $api
        ])
        ->json('GET', '/api/v1/transactions/latest');

        $response->assertJson([
            'authenticated' => true,
            'success'       => true,
            'result'        => [
                'id'         => 1,
                'client_id'   => null,
                'parent_id'   => null,
                'terminal_id' => 1,
                'user_id'     => 1,
                'currency'    => 'GBP',
                'status'      => 'complete'
            ],
        ]);
    }

    public function dataPost()
    {
        return [
            'Empty post' => [
                [],
                [],
                [],
                []
            ],
            'Single item' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'product_id' => '1',
                                'name' => 'Hound of the Baskervilles',
                                'short_name' => 'Hound Baskervilles',
                                'tax' => [
                                    'rate' => 'gb_vat_exempt',
                                    'label' => 'British VAT Exempt',
                                    'percent' => '0'
                                ],
                                'qty' => 1,
                                'single_amount' => '99.99',
                                'line_amount' => '99.99',
                                'line_tax' => '0',
                                'type' => 'product',
                                'created_at' => '2019-01-20T21:29:36.513Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => 'Total',
                                'short_name' => 'Total',
                                'qty' => 1,
                                'single_amount' => '99.99',
                                'line_amount' => '99.99',
                                'line_tax' => '0',
                                'type' => 'total',
                                'created_at' => '2019-01-20T21:29:49.743Z'
                            ],
                            [
                                'local_id' => 3,
                                'name' => 'VAT',
                                'short_name' => 'VAT',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'tax',
                                'created_at' => '2019-01-20T21:29:49.744Z'
                            ],
                            [
                                'local_id' => 4,
                                'payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => -1,
                                'single_amount' => '100',
                                'line_amount' => '-100',
                                'line_tax' => '0',
                                'type' => 'payment',
                                'created_at' => '2019-01-20T22:00:53.358Z'
                            ],
                            [
                                'local_id' => 5,
                                'name' => 'Change',
                                'short_name' => 'Change',
                                'qty' => 1,
                                'single_amount' => '0.01',
                                'line_amount' => '0.01',
                                'line_tax' => '0',
                                'type' => 'change',
                                'created_at' => '2019-01-20T22:00:53.359Z'
                            ]
                        ],
                        'local_id'   => 332,
                        'status'     => 'complete',
                        'currency'   => 'GBP',
                        'user_id'    => 2,
                        'created_at' => '2019-01-20T21:02:57.977Z',
                    ]
                ],
                [
                    [
                        'client_id'   => 332,
                        'parent_id'   => null,
                        'terminal_id' => 1,
                        'user_id'     => 2,
                        'currency'    => 'GBP',
                        'status'      => 'complete',
                        'created_at'  => new Carbon('2019-01-20T21:02:57.977Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Product::class,
                            'name'           => 'Hound of the Baskervilles',
                            'short_name'     => 'Hound Baskervilles',
                            'tax_rate'       => 'gb_vat_exempt',
                            'qty'            => 1,
                            'single_amount'  => 99.99,
                            'line_amount'    => 99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:36.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Total::class,
                            'name'           => 'Total',
                            'short_name'     => 'Total',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 99.99,
                            'line_amount'    => 99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:49.743Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Tax::class,
                            'name'           => 'VAT',
                            'short_name'     => 'VAT',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:49.744Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Payment::class,
                            'name'           => 'Cash',
                            'short_name'     => 'Cash',
                            'tax_rate'       => null,
                            'qty'            => -1,
                            'single_amount'  => 100,
                            'line_amount'    => -100,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T22:00:53.358Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Change::class,
                            'name'           => 'Change',
                            'short_name'     => 'Change',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0.01,
                            'line_amount'    => 0.01,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T22:00:53.359Z')
                        ]
                    ]
                ],
                [
                    [
                        [
                            'id'              => 1,
                            'payment_type_id' => 1,
                            'amount'          => 99.99
                        ]
                    ]
                ]
            ],
            'Refund' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'product_id' => 1,
                                'name' => 'Hound of the Baskervilles',
                                'short_name' => 'Hound Baskervilles',
                                'tax' => [
                                    'rate' => 'gb_vat_exempt',
                                    'label' => 'British VAT Exempt',
                                    'percent' => '0'
                                ],
                                'qty' => -1,
                                'single_amount' => '99.99',
                                'line_amount' => '-99.99',
                                'line_tax' => '-0',
                                'type' => 'product',
                                'created_at' => '2019-01-23T21:37:43.166Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => 'Total',
                                'short_name' => 'Total',
                                'qty' => 1,
                                'single_amount' => '-99.99',
                                'line_amount' => '-99.99',
                                'line_tax' => '0',
                                'type' => 'total',
                                'created_at' => '2019-01-23T21:37:48.044Z'
                            ],
                            [
                                'local_id' => 3,
                                'name' => 'VAT',
                                'short_name' => 'VAT',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'tax',
                                'created_at' => '2019-01-23T21:37:48.046Z'
                            ],
                            [
                                'local_id' => 4,
                                'payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => -1,
                                'single_amount' => '-99.99',
                                'line_amount' => '99.99',
                                'line_tax' => '0',
                                'type' => 'payment',
                                'created_at' => '2019-01-23T21:37:48.046Z'
                            ],
                            [
                                'local_id' => 5,
                                'name' => 'Change',
                                'short_name' => 'Change',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'change',
                                'created_at' => '2019-01-23T21:37:48.048Z'
                            ]
                        ],
                        'local_id'   => 196,
                        'status'     => 'complete',
                        'currency'   => 'GBP',
                        'user_id'    => 3,
                        'created_at' => '2019-01-23T21:37:34.119Z',
                    ]
                ],
                [
                    [
                        'client_id'   => 196,
                        'parent_id'   => null,
                        'terminal_id' => 1,
                        'user_id'     => 3,
                        'currency'    => 'GBP',
                        'status'      => 'complete',
                        'created_at'  => new Carbon('2019-01-23T21:37:34.119Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Product::class,
                            'name'           => 'Hound of the Baskervilles',
                            'short_name'     => 'Hound Baskervilles',
                            'tax_rate'       => 'gb_vat_exempt',
                            'qty'            => -1,
                            'single_amount'  => 99.99,
                            'line_amount'    => -99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:43.166Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Total::class,
                            'name'           => 'Total',
                            'short_name'     => 'Total',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => -99.99,
                            'line_amount'    => -99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:48.044Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Tax::class,
                            'name'           => 'VAT',
                            'short_name'     => 'VAT',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:48.046Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Payment::class,
                            'name'           => 'Cash',
                            'short_name'     => 'Cash',
                            'tax_rate'       => null,
                            'qty'            => -1,
                            'single_amount'  => -99.99,
                            'line_amount'    => 99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:48.046Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Change::class,
                            'name'           => 'Change',
                            'short_name'     => 'Change',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:48.048Z')
                        ]
                    ]
                ],
                [
                    [
                        [
                            'id'              => 1,
                            'payment_type_id' => 1,
                            'amount'          => -99.99
                        ]
                    ]
                ]
            ],
            'Voided' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'product_id' => '1',
                                'name' => 'Hound of the Baskervilles',
                                'short_name' => 'Hound Baskervilles',
                                'tax' => [
                                    'rate' => 'gb_vat_exempt',
                                    'label' => 'British VAT Exempt',
                                    'percent' => '0'
                                ],
                                'qty' => 1,
                                'single_amount' => '99.99',
                                'line_amount' => '99.99',
                                'line_tax' => '0',
                                'type' => 'product',
                                'created_at' => '2019-01-20T21:29:36.513Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => '-- Voided --',
                                'short_name' => '-- VOID --',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'void',
                                'created_at' => '2019-01-20T21:29:49.743Z'
                            ],
                        ],
                        'local_id'   => 332,
                        'status'     => 'void',
                        'currency'   => 'GBP',
                        'user_id'    => 2,
                        'created_at' => '2019-01-20T21:02:57.977Z',
                    ]
                ],
                [
                    [
                        'client_id'   => 332,
                        'parent_id'   => null,
                        'terminal_id' => 1,
                        'user_id'     => 2,
                        'currency'    => 'GBP',
                        'status'      => 'void',
                        'created_at'  => new Carbon('2019-01-20T21:02:57.977Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Product::class,
                            'name'           => 'Hound of the Baskervilles',
                            'short_name'     => 'Hound Baskervilles',
                            'tax_rate'       => 'gb_vat_exempt',
                            'qty'            => 1,
                            'single_amount'  => 99.99,
                            'line_amount'    => 99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:36.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => VoidTransaction::class,
                            'name'           => '-- Voided --',
                            'short_name'     => '-- VOID --',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:49.743Z')
                        ],
                    ]
                ],
                [[]]
            ],
            'Void after part payment' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'product_id' => '1',
                                'name' => 'Hound of the Baskervilles',
                                'short_name' => 'Hound Baskervilles',
                                'tax' => [
                                    'rate' => 'gb_vat_exempt',
                                    'label' => 'British VAT Exempt',
                                    'percent' => '0'
                                ],
                                'qty' => 1,
                                'single_amount' => '99.99',
                                'line_amount' => '99.99',
                                'line_tax' => '0',
                                'type' => 'product',
                                'created_at' => '2019-01-20T21:29:36.513Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => 'Total',
                                'short_name' => 'Total',
                                'qty' => 1,
                                'single_amount' => '99.99',
                                'line_amount' => '99.99',
                                'line_tax' => '0',
                                'type' => 'total',
                                'created_at' => '2019-01-20T21:29:49.743Z'
                            ],
                            [
                                'local_id' => 3,
                                'name' => 'VAT',
                                'short_name' => 'VAT',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'tax',
                                'created_at' => '2019-01-20T21:29:49.744Z'
                            ],
                            [
                                'local_id' => 4,
                                'payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => -1,
                                'single_amount' => '50',
                                'line_amount' => '-50',
                                'line_tax' => '0',
                                'type' => 'payment',
                                'created_at' => '2019-01-20T22:00:53.358Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => '-- Voided --',
                                'short_name' => '-- VOID --',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'void',
                                'created_at' => '2019-01-20T22:29:49.743Z'
                            ],
                        ],
                        'local_id'   => 333,
                        'status'     => 'void',
                        'currency'   => 'GBP',
                        'user_id'    => 2,
                        'created_at' => '2019-01-20T21:02:57.977Z',
                    ]
                ],
                [
                    [
                        'client_id'   => 333,
                        'parent_id'   => null,
                        'terminal_id' => 1,
                        'user_id'     => 2,
                        'currency'    => 'GBP',
                        'status'      => 'void',
                        'created_at'  => new Carbon('2019-01-20T21:02:57.977Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Product::class,
                            'name'           => 'Hound of the Baskervilles',
                            'short_name'     => 'Hound Baskervilles',
                            'tax_rate'       => 'gb_vat_exempt',
                            'qty'            => 1,
                            'single_amount'  => 99.99,
                            'line_amount'    => 99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:36.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Total::class,
                            'name'           => 'Total',
                            'short_name'     => 'Total',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 99.99,
                            'line_amount'    => 99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:49.743Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Tax::class,
                            'name'           => 'VAT',
                            'short_name'     => 'VAT',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:49.744Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Payment::class,
                            'name'           => 'Cash',
                            'short_name'     => 'Cash',
                            'tax_rate'       => null,
                            'qty'            => -1,
                            'single_amount'  => 50,
                            'line_amount'    => -50,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T22:00:53.358Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => VoidTransaction::class,
                            'name'           => '-- Voided --',
                            'short_name'     => '-- VOID --',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T22:29:49.743Z')
                        ]
                    ]
                ],
                [
                    [
                        [
                            'id'              => 1,
                            'payment_type_id' => 1,
                            'amount'          => 0
                        ]
                    ]
                ]
            ],
            'No Sale' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'user_id' => 3,
                                'name' => 'Authorisation by Tobias',
                                'short_name' => 'By Tobias',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'auth',
                                'created_at' => '2019-01-20T21:29:36.513Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => 'No Sale',
                                'short_name' => 'No Sale',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'action',
                                'created_at' => '2019-01-20T21:29:36.514Z'
                            ],
                        ],
                        'local_id'   => 334,
                        'status'     => 'complete',
                        'currency'   => 'GBP',
                        'user_id'    => 1,
                        'created_at' => '2019-01-20T21:02:57.977Z',
                    ]
                ],
                [
                    [
                        'client_id'   => 334,
                        'parent_id'   => null,
                        'terminal_id' => 1,
                        'user_id'     => 1,
                        'currency'    => 'GBP',
                        'status'      => 'complete',
                        'created_at'  => new Carbon('2019-01-20T21:02:57.977Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 3,
                            'object_type'    => User::class,
                            'name'           => 'Authorisation by Tobias',
                            'short_name'     => 'By Tobias',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:36.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Action::class,
                            'name'           => 'No Sale',
                            'short_name'     => 'No Sale',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T21:29:36.514Z')
                        ],
                    ]
                ],
                [[]]
            ],
            'EOD' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'user_id' => 3,
                                'name' => 'Authorisation by Tobias',
                                'short_name' => 'By Tobias',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'auth',
                                'created_at' => '2019-01-20T20:21:27.417Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => 'End of Day',
                                'short_name' => 'End of Day',
                                'qty' => 0,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod',
                                'created_at' => '2019-01-20T20:21:27.418Z'
                            ],
                            [
                                'local_id' => 3,
                                'eod_payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-label',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 4,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 50.00',
                                'short_name' => ' 0 x 50.00',
                                'qty' => 0,
                                'single_amount' => '50',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 5,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 2 x 20.00',
                                'short_name' => ' 2 x 20.00',
                                'qty' => 2,
                                'single_amount' => '20',
                                'line_amount' => '40',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 6,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 2 x 10.00',
                                'short_name' => ' 2 x 10.00',
                                'qty' => 2,
                                'single_amount' => '10',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 7,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 10 x 5.00',
                                'short_name' => ' 10 x 5.00',
                                'qty' => 10,
                                'single_amount' => '5',
                                'line_amount' => '50',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 8,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 5 x 2.00',
                                'short_name' => ' 5 x 2.00',
                                'qty' => 5,
                                'single_amount' => '2',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 9,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 28 x 1.00',
                                'short_name' => ' 28 x 1.00',
                                'qty' => 28,
                                'single_amount' => '1',
                                'line_amount' => '28',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 10,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 24 x 0.50',
                                'short_name' => ' 24 x 0.50',
                                'qty' => 24,
                                'single_amount' => '0.5',
                                'line_amount' => '12',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 11,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 55 x 0.20',
                                'short_name' => ' 55 x 0.20',
                                'qty' => 55,
                                'single_amount' => '0.2',
                                'line_amount' => '11',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 12,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 90 x 0.10',
                                'short_name' => ' 90 x 0.10',
                                'qty' => 90,
                                'single_amount' => '0.1',
                                'line_amount' => '9',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 13,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 100 x 0.05',
                                'short_name' => ' 100 x 0.05',
                                'qty' => 100,
                                'single_amount' => '0.05',
                                'line_amount' => '5',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 14,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 5 x 0.02',
                                'short_name' => ' 5 x 0.02',
                                'qty' => 5,
                                'single_amount' => '0.02',
                                'line_amount' => '0.1',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 15,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 0.01',
                                'short_name' => ' 0 x 0.01',
                                'qty' => 0,
                                'single_amount' => '0.01',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 16,
                                'eod_payment_id' => 2,
                                'eod_mode' => 'amount',
                                'name' => 'Card',
                                'short_name' => 'Card',
                                'qty' => 1,
                                'single_amount' => '10',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 17,
                                'eod_payment_id' => 3,
                                'eod_mode' => 'amount',
                                'name' => 'Voucher',
                                'short_name' => 'Voucher',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.515Z'
                            ],
                            [
                                'local_id' => 18,
                                'eod_payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-label',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 19,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 50.00',
                                'short_name' => ' 0 x 50.00',
                                'qty' => 0,
                                'single_amount' => '50',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 20,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 20.00',
                                'short_name' => ' 0 x 20.00',
                                'qty' => 0,
                                'single_amount' => '20',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 21,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 2 x 10.00',
                                'short_name' => ' 2 x 10.00',
                                'qty' => 2,
                                'single_amount' => '10',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 22,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 4 x 5.00',
                                'short_name' => ' 4 x 5.00',
                                'qty' => 4,
                                'single_amount' => '5',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 23,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 5 x 2.00',
                                'short_name' => ' 5 x 2.00',
                                'qty' => 5,
                                'single_amount' => '2',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 24,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 20 x 1.00',
                                'short_name' => ' 20 x 1.00',
                                'qty' => 20,
                                'single_amount' => '1',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 25,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 20 x 0.50',
                                'short_name' => ' 20 x 0.50',
                                'qty' => 20,
                                'single_amount' => '0.5',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 26,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 50 x 0.20',
                                'short_name' => ' 50 x 0.20',
                                'qty' => 50,
                                'single_amount' => '0.2',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 27,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 90 x 0.10',
                                'short_name' => ' 90 x 0.10',
                                'qty' => 90,
                                'single_amount' => '0.1',
                                'line_amount' => '9',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 28,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 20 x 0.05',
                                'short_name' => ' 20 x 0.05',
                                'qty' => 20,
                                'single_amount' => '0.05',
                                'line_amount' => '1',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 29,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 0.02',
                                'short_name' => ' 0 x 0.02',
                                'qty' => 0,
                                'single_amount' => '0.02',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 30,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 0.01',
                                'short_name' => ' 0 x 0.01',
                                'qty' => 0,
                                'single_amount' => '0.01',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ]
                        ],
                        'local_id'   => 187,
                        'status'     => 'complete',
                        'currency'   => 'GBP',
                        'user_id'    => 1,
                        'created_at' => '2019-01-20T20:21:22.154Z',
                        'noReceipt'  => true,
                        'eod'        => [
                            'start' => [
                                '1' => "100",
                                '2' => "0",
                                '3' => "0",
                            ],
                            'expected' => [
                                '1' => "200",
                                '2' => "10",
                                '3' => "0",
                            ],
                            'actual' => [
                                '1' => "185.1",
                                '2' => "10",
                                '3' => "0",
                            ],
                            'carry' => [
                                '1' => "100",
                                '2' => "0",
                                '3' => "0",
                            ]
                        ]
                    ]
                ],
                [
                    [
                        'client_id'   => 187,
                        'parent_id'   => null,
                        'terminal_id' => 1,
                        'user_id'     => 1,
                        'currency'    => 'GBP',
                        'status'      => 'complete',
                        'created_at'  => new Carbon('2019-01-20T20:21:22.154Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 3,
                            'object_type'    => User::class,
                            'name'           => 'Authorisation by Tobias',
                            'short_name'     => 'By Tobias',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.417Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Eod::class,
                            'name'           => 'End of Day',
                            'short_name'     => 'End of Day',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.418Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => EodLabel::class,
                            'name'           => 'Cash',
                            'short_name'     => 'Cash',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '0 x 50.00',
                            'short_name'     => '0 x 50.00',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 50,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '2 x 20.00',
                            'short_name'     => '2 x 20.00',
                            'tax_rate'       => null,
                            'qty'            => 2,
                            'single_amount'  => 20,
                            'line_amount'    => 40,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '2 x 10.00',
                            'short_name'     => '2 x 10.00',
                            'tax_rate'       => null,
                            'qty'            => 2,
                            'single_amount'  => 10,
                            'line_amount'    => 20,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '10 x 5.00',
                            'short_name'     => '10 x 5.00',
                            'tax_rate'       => null,
                            'qty'            => 10,
                            'single_amount'  => 5,
                            'line_amount'    => 50,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '5 x 2.00',
                            'short_name'     => '5 x 2.00',
                            'tax_rate'       => null,
                            'qty'            => 5,
                            'single_amount'  => 2,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '28 x 1.00',
                            'short_name'     => '28 x 1.00',
                            'tax_rate'       => null,
                            'qty'            => 28,
                            'single_amount'  => 1,
                            'line_amount'    => 28,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '24 x 0.50',
                            'short_name'     => '24 x 0.50',
                            'tax_rate'       => null,
                            'qty'            => 24,
                            'single_amount'  => 0.5,
                            'line_amount'    => 12,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '55 x 0.20',
                            'short_name'     => '55 x 0.20',
                            'tax_rate'       => null,
                            'qty'            => 55,
                            'single_amount'  => 0.2,
                            'line_amount'    => 11,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '90 x 0.10',
                            'short_name'     => '90 x 0.10',
                            'tax_rate'       => null,
                            'qty'            => 90,
                            'single_amount'  => 0.1,
                            'line_amount'    => 9,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '100 x 0.05',
                            'short_name'     => '100 x 0.05',
                            'tax_rate'       => null,
                            'qty'            => 100,
                            'single_amount'  => 0.05,
                            'line_amount'    => 5,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '5 x 0.02',
                            'short_name'     => '5 x 0.02',
                            'tax_rate'       => null,
                            'qty'            => 5,
                            'single_amount'  => 0.02,
                            'line_amount'    => 0.1,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '0 x 0.01',
                            'short_name'     => '0 x 0.01',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 0.01,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 2,
                            'object_type'    => EodPayment::class,
                            'name'           => 'Card',
                            'short_name'     => 'Card',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 10,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 3,
                            'object_type'    => EodPayment::class,
                            'name'           => 'Voucher',
                            'short_name'     => 'Voucher',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.515Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => EodLabel::class,
                            'name'           => 'Cash',
                            'short_name'     => 'Cash',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.196Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '0 x 50.00',
                            'short_name'     => '0 x 50.00',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 50,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.196Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '0 x 20.00',
                            'short_name'     => '0 x 20.00',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 20,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.196Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '2 x 10.00',
                            'short_name'     => '2 x 10.00',
                            'tax_rate'       => null,
                            'qty'            => 2,
                            'single_amount'  => 10,
                            'line_amount'    => 20,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.196Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '4 x 5.00',
                            'short_name'     => '4 x 5.00',
                            'tax_rate'       => null,
                            'qty'            => 4,
                            'single_amount'  => 5,
                            'line_amount'    => 20,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '5 x 2.00',
                            'short_name'     => '5 x 2.00',
                            'tax_rate'       => null,
                            'qty'            => 5,
                            'single_amount'  => 2,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '20 x 1.00',
                            'short_name'     => '20 x 1.00',
                            'tax_rate'       => null,
                            'qty'            => 20,
                            'single_amount'  => 1,
                            'line_amount'    => 20,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '20 x 0.50',
                            'short_name'     => '20 x 0.50',
                            'tax_rate'       => null,
                            'qty'            => 20,
                            'single_amount'  => 0.5,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '50 x 0.20',
                            'short_name'     => '50 x 0.20',
                            'tax_rate'       => null,
                            'qty'            => 50,
                            'single_amount'  => 0.2,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '90 x 0.10',
                            'short_name'     => '90 x 0.10',
                            'tax_rate'       => null,
                            'qty'            => 90,
                            'single_amount'  => 0.1,
                            'line_amount'    => 9,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '20 x 0.05',
                            'short_name'     => '20 x 0.05',
                            'tax_rate'       => null,
                            'qty'            => 20,
                            'single_amount'  => 0.05,
                            'line_amount'    => 1,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '0 x 0.02',
                            'short_name'     => '0 x 0.02',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 0.02,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '0 x 0.01',
                            'short_name'     => '0 x 0.01',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 0.01,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                    ]
                ],
                [[]]
            ],
            'EOD 2x' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'user_id' => 3,
                                'name' => 'Authorisation by Tobias',
                                'short_name' => 'By Tobias',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'auth',
                                'created_at' => '2019-01-20T20:21:27.417Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => 'End of Day',
                                'short_name' => 'End of Day',
                                'qty' => 0,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod',
                                'created_at' => '2019-01-20T20:21:27.418Z'
                            ],
                            [
                                'local_id' => 3,
                                'eod_payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-label',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 4,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 50.00',
                                'short_name' => ' 0 x 50.00',
                                'qty' => 0,
                                'single_amount' => '50',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 5,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 2 x 20.00',
                                'short_name' => ' 2 x 20.00',
                                'qty' => 2,
                                'single_amount' => '20',
                                'line_amount' => '40',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 6,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 2 x 10.00',
                                'short_name' => ' 2 x 10.00',
                                'qty' => 2,
                                'single_amount' => '10',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 7,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 10 x 5.00',
                                'short_name' => ' 10 x 5.00',
                                'qty' => 10,
                                'single_amount' => '5',
                                'line_amount' => '50',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 8,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 5 x 2.00',
                                'short_name' => ' 5 x 2.00',
                                'qty' => 5,
                                'single_amount' => '2',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 9,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 28 x 1.00',
                                'short_name' => ' 28 x 1.00',
                                'qty' => 28,
                                'single_amount' => '1',
                                'line_amount' => '28',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 10,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 24 x 0.50',
                                'short_name' => ' 24 x 0.50',
                                'qty' => 24,
                                'single_amount' => '0.5',
                                'line_amount' => '12',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 11,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 55 x 0.20',
                                'short_name' => ' 55 x 0.20',
                                'qty' => 55,
                                'single_amount' => '0.2',
                                'line_amount' => '11',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 12,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 90 x 0.10',
                                'short_name' => ' 90 x 0.10',
                                'qty' => 90,
                                'single_amount' => '0.1',
                                'line_amount' => '9',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 13,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 100 x 0.05',
                                'short_name' => ' 100 x 0.05',
                                'qty' => 100,
                                'single_amount' => '0.05',
                                'line_amount' => '5',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 14,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 5 x 0.02',
                                'short_name' => ' 5 x 0.02',
                                'qty' => 5,
                                'single_amount' => '0.02',
                                'line_amount' => '0.1',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 15,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 0.01',
                                'short_name' => ' 0 x 0.01',
                                'qty' => 0,
                                'single_amount' => '0.01',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 16,
                                'eod_payment_id' => 2,
                                'eod_mode' => 'amount',
                                'name' => 'Card',
                                'short_name' => 'Card',
                                'qty' => 1,
                                'single_amount' => '10',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 17,
                                'eod_payment_id' => 3,
                                'eod_mode' => 'amount',
                                'name' => 'Voucher',
                                'short_name' => 'Voucher',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.515Z'
                            ],
                            [
                                'local_id' => 18,
                                'eod_payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-label',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 19,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 50.00',
                                'short_name' => ' 0 x 50.00',
                                'qty' => 0,
                                'single_amount' => '50',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 20,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 20.00',
                                'short_name' => ' 0 x 20.00',
                                'qty' => 0,
                                'single_amount' => '20',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 21,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 2 x 10.00',
                                'short_name' => ' 2 x 10.00',
                                'qty' => 2,
                                'single_amount' => '10',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 22,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 4 x 5.00',
                                'short_name' => ' 4 x 5.00',
                                'qty' => 4,
                                'single_amount' => '5',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 23,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 5 x 2.00',
                                'short_name' => ' 5 x 2.00',
                                'qty' => 5,
                                'single_amount' => '2',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 24,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 20 x 1.00',
                                'short_name' => ' 20 x 1.00',
                                'qty' => 20,
                                'single_amount' => '1',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 25,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 20 x 0.50',
                                'short_name' => ' 20 x 0.50',
                                'qty' => 20,
                                'single_amount' => '0.5',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 26,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 50 x 0.20',
                                'short_name' => ' 50 x 0.20',
                                'qty' => 50,
                                'single_amount' => '0.2',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 27,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 90 x 0.10',
                                'short_name' => ' 90 x 0.10',
                                'qty' => 90,
                                'single_amount' => '0.1',
                                'line_amount' => '9',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 28,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 20 x 0.05',
                                'short_name' => ' 20 x 0.05',
                                'qty' => 20,
                                'single_amount' => '0.05',
                                'line_amount' => '1',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 29,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 0.02',
                                'short_name' => ' 0 x 0.02',
                                'qty' => 0,
                                'single_amount' => '0.02',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 30,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 0.01',
                                'short_name' => ' 0 x 0.01',
                                'qty' => 0,
                                'single_amount' => '0.01',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ]
                        ],
                        'local_id'   => 187,
                        'status'     => 'complete',
                        'currency'   => 'GBP',
                        'user_id'    => 1,
                        'created_at' => '2019-01-20T20:21:22.154Z',
                        'noReceipt'  => true,
                        'eod'        => [
                            'start' => [
                                '1' => "100",
                                '2' => "0",
                                '3' => "0",
                            ],
                            'expected' => [
                                '1' => "200",
                                '2' => "10",
                                '3' => "0",
                            ],
                            'actual' => [
                                '1' => "185.1",
                                '2' => "10",
                                '3' => "0",
                            ],
                            'carry' => [
                                '1' => "100",
                                '2' => "0",
                                '3' => "0",
                            ]
                        ]
                    ],
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'user_id' => 3,
                                'name' => 'Authorisation by Tobias',
                                'short_name' => 'By Tobias',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'auth',
                                'created_at' => '2019-01-20T20:21:27.417Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => 'End of Day',
                                'short_name' => 'End of Day',
                                'qty' => 0,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod',
                                'created_at' => '2019-01-20T20:21:27.418Z'
                            ],
                            [
                                'local_id' => 3,
                                'eod_payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-label',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 4,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 50.00',
                                'short_name' => ' 0 x 50.00',
                                'qty' => 0,
                                'single_amount' => '50',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 5,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 2 x 20.00',
                                'short_name' => ' 2 x 20.00',
                                'qty' => 2,
                                'single_amount' => '20',
                                'line_amount' => '40',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 6,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 2 x 10.00',
                                'short_name' => ' 2 x 10.00',
                                'qty' => 2,
                                'single_amount' => '10',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.513Z'
                            ],
                            [
                                'local_id' => 7,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 10 x 5.00',
                                'short_name' => ' 10 x 5.00',
                                'qty' => 10,
                                'single_amount' => '5',
                                'line_amount' => '50',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 8,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 5 x 2.00',
                                'short_name' => ' 5 x 2.00',
                                'qty' => 5,
                                'single_amount' => '2',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 9,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 28 x 1.00',
                                'short_name' => ' 28 x 1.00',
                                'qty' => 28,
                                'single_amount' => '1',
                                'line_amount' => '28',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 10,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 24 x 0.50',
                                'short_name' => ' 24 x 0.50',
                                'qty' => 24,
                                'single_amount' => '0.5',
                                'line_amount' => '12',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 11,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 55 x 0.20',
                                'short_name' => ' 55 x 0.20',
                                'qty' => 55,
                                'single_amount' => '0.2',
                                'line_amount' => '11',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 12,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 90 x 0.10',
                                'short_name' => ' 90 x 0.10',
                                'qty' => 90,
                                'single_amount' => '0.1',
                                'line_amount' => '9',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 13,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 100 x 0.05',
                                'short_name' => ' 100 x 0.05',
                                'qty' => 100,
                                'single_amount' => '0.05',
                                'line_amount' => '5',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 14,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 5 x 0.02',
                                'short_name' => ' 5 x 0.02',
                                'qty' => 5,
                                'single_amount' => '0.02',
                                'line_amount' => '0.1',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 15,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 0.01',
                                'short_name' => ' 0 x 0.01',
                                'qty' => 0,
                                'single_amount' => '0.01',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 16,
                                'eod_payment_id' => 2,
                                'eod_mode' => 'amount',
                                'name' => 'Card',
                                'short_name' => 'Card',
                                'qty' => 1,
                                'single_amount' => '10',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.514Z'
                            ],
                            [
                                'local_id' => 17,
                                'eod_payment_id' => 3,
                                'eod_mode' => 'amount',
                                'name' => 'Voucher',
                                'short_name' => 'Voucher',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-payment',
                                'created_at' => '2019-01-20T20:21:27.515Z'
                            ],
                            [
                                'local_id' => 18,
                                'eod_payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-label',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 19,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 50.00',
                                'short_name' => ' 0 x 50.00',
                                'qty' => 0,
                                'single_amount' => '50',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 20,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 20.00',
                                'short_name' => ' 0 x 20.00',
                                'qty' => 0,
                                'single_amount' => '20',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 21,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 2 x 10.00',
                                'short_name' => ' 2 x 10.00',
                                'qty' => 2,
                                'single_amount' => '10',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 22,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 4 x 5.00',
                                'short_name' => ' 4 x 5.00',
                                'qty' => 4,
                                'single_amount' => '5',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.196Z'
                            ],
                            [
                                'local_id' => 23,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 5 x 2.00',
                                'short_name' => ' 5 x 2.00',
                                'qty' => 5,
                                'single_amount' => '2',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 24,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 20 x 1.00',
                                'short_name' => ' 20 x 1.00',
                                'qty' => 20,
                                'single_amount' => '1',
                                'line_amount' => '20',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 25,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 20 x 0.50',
                                'short_name' => ' 20 x 0.50',
                                'qty' => 20,
                                'single_amount' => '0.5',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 26,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 50 x 0.20',
                                'short_name' => ' 50 x 0.20',
                                'qty' => 50,
                                'single_amount' => '0.2',
                                'line_amount' => '10',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 27,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 90 x 0.10',
                                'short_name' => ' 90 x 0.10',
                                'qty' => 90,
                                'single_amount' => '0.1',
                                'line_amount' => '9',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 28,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 20 x 0.05',
                                'short_name' => ' 20 x 0.05',
                                'qty' => 20,
                                'single_amount' => '0.05',
                                'line_amount' => '1',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 29,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 0.02',
                                'short_name' => ' 0 x 0.02',
                                'qty' => 0,
                                'single_amount' => '0.02',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ],
                            [
                                'local_id' => 30,
                                'eod_payment_id' => 1,
                                'eod_mode' => 'qty',
                                'name' => ' 0 x 0.01',
                                'short_name' => ' 0 x 0.01',
                                'qty' => 0,
                                'single_amount' => '0.01',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'eod-carry',
                                'created_at' => '2019-01-20T20:21:35.197Z'
                            ]
                        ],
                        'local_id'   => 187,
                        'status'     => 'complete',
                        'currency'   => 'GBP',
                        'user_id'    => 1,
                        'created_at' => '2019-01-20T20:21:22.154Z',
                        'noReceipt'  => true,
                        'eod'        => [
                            'start' => [
                                '1' => "100",
                                '2' => "0",
                                '3' => "0",
                            ],
                            'expected' => [
                                '1' => "200",
                                '2' => "10",
                                '3' => "0",
                            ],
                            'actual' => [
                                '1' => "185.1",
                                '2' => "10",
                                '3' => "0",
                            ],
                            'carry' => [
                                '1' => "100",
                                '2' => "0",
                                '3' => "0",
                            ]
                        ]
                    ]
                ],
                [
                    [
                        'client_id'   => 187,
                        'parent_id'   => null,
                        'terminal_id' => 1,
                        'user_id'     => 1,
                        'currency'    => 'GBP',
                        'status'      => 'complete',
                        'created_at'  => new Carbon('2019-01-20T20:21:22.154Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 3,
                            'object_type'    => User::class,
                            'name'           => 'Authorisation by Tobias',
                            'short_name'     => 'By Tobias',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.417Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Eod::class,
                            'name'           => 'End of Day',
                            'short_name'     => 'End of Day',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.418Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => EodLabel::class,
                            'name'           => 'Cash',
                            'short_name'     => 'Cash',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '0 x 50.00',
                            'short_name'     => '0 x 50.00',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 50,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '2 x 20.00',
                            'short_name'     => '2 x 20.00',
                            'tax_rate'       => null,
                            'qty'            => 2,
                            'single_amount'  => 20,
                            'line_amount'    => 40,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '2 x 10.00',
                            'short_name'     => '2 x 10.00',
                            'tax_rate'       => null,
                            'qty'            => 2,
                            'single_amount'  => 10,
                            'line_amount'    => 20,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.513Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '10 x 5.00',
                            'short_name'     => '10 x 5.00',
                            'tax_rate'       => null,
                            'qty'            => 10,
                            'single_amount'  => 5,
                            'line_amount'    => 50,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '5 x 2.00',
                            'short_name'     => '5 x 2.00',
                            'tax_rate'       => null,
                            'qty'            => 5,
                            'single_amount'  => 2,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '28 x 1.00',
                            'short_name'     => '28 x 1.00',
                            'tax_rate'       => null,
                            'qty'            => 28,
                            'single_amount'  => 1,
                            'line_amount'    => 28,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '24 x 0.50',
                            'short_name'     => '24 x 0.50',
                            'tax_rate'       => null,
                            'qty'            => 24,
                            'single_amount'  => 0.5,
                            'line_amount'    => 12,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '55 x 0.20',
                            'short_name'     => '55 x 0.20',
                            'tax_rate'       => null,
                            'qty'            => 55,
                            'single_amount'  => 0.2,
                            'line_amount'    => 11,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '90 x 0.10',
                            'short_name'     => '90 x 0.10',
                            'tax_rate'       => null,
                            'qty'            => 90,
                            'single_amount'  => 0.1,
                            'line_amount'    => 9,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '100 x 0.05',
                            'short_name'     => '100 x 0.05',
                            'tax_rate'       => null,
                            'qty'            => 100,
                            'single_amount'  => 0.05,
                            'line_amount'    => 5,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '5 x 0.02',
                            'short_name'     => '5 x 0.02',
                            'tax_rate'       => null,
                            'qty'            => 5,
                            'single_amount'  => 0.02,
                            'line_amount'    => 0.1,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodPayment::class,
                            'name'           => '0 x 0.01',
                            'short_name'     => '0 x 0.01',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 0.01,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 2,
                            'object_type'    => EodPayment::class,
                            'name'           => 'Card',
                            'short_name'     => 'Card',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 10,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.514Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 3,
                            'object_type'    => EodPayment::class,
                            'name'           => 'Voucher',
                            'short_name'     => 'Voucher',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:27.515Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => EodLabel::class,
                            'name'           => 'Cash',
                            'short_name'     => 'Cash',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.196Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '0 x 50.00',
                            'short_name'     => '0 x 50.00',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 50,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.196Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '0 x 20.00',
                            'short_name'     => '0 x 20.00',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 20,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.196Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '2 x 10.00',
                            'short_name'     => '2 x 10.00',
                            'tax_rate'       => null,
                            'qty'            => 2,
                            'single_amount'  => 10,
                            'line_amount'    => 20,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.196Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '4 x 5.00',
                            'short_name'     => '4 x 5.00',
                            'tax_rate'       => null,
                            'qty'            => 4,
                            'single_amount'  => 5,
                            'line_amount'    => 20,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '5 x 2.00',
                            'short_name'     => '5 x 2.00',
                            'tax_rate'       => null,
                            'qty'            => 5,
                            'single_amount'  => 2,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '20 x 1.00',
                            'short_name'     => '20 x 1.00',
                            'tax_rate'       => null,
                            'qty'            => 20,
                            'single_amount'  => 1,
                            'line_amount'    => 20,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '20 x 0.50',
                            'short_name'     => '20 x 0.50',
                            'tax_rate'       => null,
                            'qty'            => 20,
                            'single_amount'  => 0.5,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '50 x 0.20',
                            'short_name'     => '50 x 0.20',
                            'tax_rate'       => null,
                            'qty'            => 50,
                            'single_amount'  => 0.2,
                            'line_amount'    => 10,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '90 x 0.10',
                            'short_name'     => '90 x 0.10',
                            'tax_rate'       => null,
                            'qty'            => 90,
                            'single_amount'  => 0.1,
                            'line_amount'    => 9,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '20 x 0.05',
                            'short_name'     => '20 x 0.05',
                            'tax_rate'       => null,
                            'qty'            => 20,
                            'single_amount'  => 0.05,
                            'line_amount'    => 1,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '0 x 0.02',
                            'short_name'     => '0 x 0.02',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 0.02,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => EodCarry::class,
                            'name'           => '0 x 0.01',
                            'short_name'     => '0 x 0.01',
                            'tax_rate'       => null,
                            'qty'            => 0,
                            'single_amount'  => 0.01,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-20T20:21:35.197Z')
                        ],
                    ]
                ],
                [[]]
            ]
        ];
    }

    /**
     * @covers       ::post
     * @dataProvider dataPost
     * @param array $postData             Transaction records posted by terminal
     * @param array $expectedTransactions Expected DB transaction records
     * @param array $expectedLines        Expected DB transaction line records, grouped by transaction
     * @param array $expectedPayments     Expected DB payment records, grouped by transaction
     */
    public function testPost(array $postData, array $expectedTransactions, array $expectedLines, array $expectedPayments)
    {
        $api = Terminal::first()->api_key;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $api
        ])
        ->json('POST', '/api/v1/transactions/', ['transactions' => $postData]);

        $response->assertJson([
            'authenticated' => true,
            'success'       => true,
        ]);

        // Check expected records exist in DB
        foreach ($expectedTransactions as $expectedTransaction) {
            $this->assertDatabaseHas('oppin_pos_transactions', $expectedTransaction);
        }
        $this->assertEquals(count($expectedTransactions), Transaction::count());
        $count = 0;
        foreach ($expectedLines as $expectedLinesTransaction) {
            foreach ($expectedLinesTransaction as $expectedLine) {
                $this->assertDatabaseHas('oppin_pos_transaction_lines', $expectedLine);
            }
            $count += count($expectedLinesTransaction);
        }
        $this->assertEquals($count, TransactionLine::count());
        $count = 0;
        foreach ($expectedPayments as $expectedPaymentTransaction) {
            foreach ($expectedPaymentTransaction as $expectedPayment) {
                $this->assertDatabaseHas('oppin_pos_payments', $expectedPayment);
            }
            $count += count($expectedPaymentTransaction);
        }
        $this->assertEquals($count, Payment::count());

        // Check EOD records
        if (!empty($postData['eod'])) {
            foreach ($postData['eod']['start'] as $type_id => $amount) {
                $expectedEod = [
                    'end_of_day_id'   => 1,
                    'payment_type_id' => $type_id,
                    'start'           => $amount,
                    'expected'        => $postData['eod']['expected'][$type_id],
                    'actual'          => $postData['eod']['actual'][$type_id],
                    'carry'           => $postData['eod']['carry'][$type_id],
                ];
                $this->assertDatabaseHas('end_of_day_payment_type', $expectedEod);
            }
        }
    }
}
