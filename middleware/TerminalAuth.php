<?php namespace Oppin\POS\Middleware;

use Closure;
use Illuminate\Http\Request;
use Oppin\POS\Models\Terminal;

class TerminalAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->setUserResolver(function($guard) use ($request) {
            $terminal = $request->bearerToken() ? Terminal::where('api_key', $request->bearerToken())->remember(10)->first() : null;
            if (!$terminal) {
                return null;
            }
            return $terminal;
        });

        if (!$request->user()) {
            return response()->json([
                'authenticated' => false,
                'success'       => false,
                'message'       => 'Client authentication failed',
            ], 401);
        }

        return $next($request);
    }
}
