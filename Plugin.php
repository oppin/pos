<?php namespace Oppin\POS;

use App;
use Backend;
use Validator as BaseValidator;
use Illuminate\Auth\AuthServiceProvider;
use System\Classes\PluginBase;
use Oppin\POS\Validator;

/**
 * POS Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var bool Plugin requires elevated permissions.
     */
    public $elevated = true;

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'POS',
            'description' => 'A retail POS system',
            'author'      => 'Oppin',
            'icon'        => 'icon-calculator'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        BaseValidator::resolver(function($translator, $data, $rules, $messages, $customAttributes) {
            return new Validator($translator, $data, $rules, $messages, $customAttributes);
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Oppin\POS\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'oppin.pos.users.manage' => [
                'tab' => 'POS Users',
                'label' => 'Manage POS Users'
            ],
            'oppin.pos.locations.manage' => [
                'tab' => 'POS Locations',
                'label' => 'Manage POS Locations'
            ],
            'oppin.pos.terminals.manage' => [
                'tab' => 'POS Terminals',
                'label' => 'Manage POS Terminals'
            ],
            'oppin.pos.reports.totals' => [
                'tab' => 'POS Reports',
                'label' => 'View Totals'
            ],
            'oppin.pos.reports.eod' => [
                'tab' => 'POS Reports',
                'label' => 'View EOD'
            ],
            'oppin.pos.reports.products' => [
                'tab' => 'POS Reports',
                'label' => 'View Products'
            ],
            'oppin.pos.reports.transactions' => [
                'tab' => 'POS Reports',
                'label' => 'View Transactions'
            ],
            'oppin.pos.access_settings' => [
                'tab' => 'POS Settings',
                'label' => 'Manage Settings'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'products' => [
                'label'       => 'Products',
                'url'         => Backend::url('oppin/pos/products'),
                'icon'        => 'icon-tag',
                'permissions' => ['oppin.pos.*'],
                'order'       => 500,
            ],
            // 'offers' => [
            //     'label'       => 'Offers',
            //     'url'         => Backend::url('oppin/pos/offers'),
            //     'icon'        => 'icon-leaf',
            //     'permissions' => ['oppin.pos.*'],
            //     'order'       => 500,
            // ],
            'users' => [
                'label'       => 'Users',
                'url'         => Backend::url('oppin/pos/users'),
                'icon'        => 'icon-users',
                'permissions' => ['oppin.pos.users.*'],
                'order'       => 550,
            ],
            'locations' => [
                'label'       => 'Locations',
                'url'         => Backend::url('oppin/pos/locations'),
                'icon'        => 'icon-building',
                'permissions' => ['oppin.pos.locations.*'],
                'order'       => 600,
            ],
            'terminals' => [
                'label'       => 'Terminals',
                'url'         => Backend::url('oppin/pos/terminals'),
                'icon'        => 'icon-television',
                'permissions' => ['oppin.pos.terminals.*'],
                'order'       => 650,
            ],
            'reports' => [
                'label'       => 'Reports',
                'url'         => Backend::url('oppin/pos/reports'),
                'icon'        => 'icon-line-chart',
                'permissions' => ['oppin.pos.reports.*'],
                'order'       => 700,
                'sideMenu'    => [
                    'totals' => [
                        'label'       => 'Totals',
                        'icon'        => 'icon-line-chart',
                        'url'         => Backend::url('oppin/pos/reports/totals'),
                        'permissions' => ['oppin.pos.reports.totals']
                    ],
                    'eod' => [
                        'label'       => 'End of Day',
                        'icon'        => 'icon-money',
                        'url'         => Backend::url('oppin/pos/reports/eods'),
                        'permissions' => ['oppin.pos.reports.eod']
                    ],
                    'products' => [
                        'label'       => 'Products',
                        'icon'        => 'icon-tag',
                        'url'         => Backend::url('oppin/pos/reports/products'),
                        'permissions' => ['oppin.pos.reports.products']
                    ],
                    'transactions' => [
                        'label'       => 'Transactions',
                        'icon'        => 'icon-shopping-bag',
                        'url'         => Backend::url('oppin/pos/reports/transactions'),
                        'permissions' => ['oppin.pos.reports.transactions']
                    ],
                ]
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'POS Settings',
                'description' => 'Manage POS settings.',
                'category'    => 'POS',
                'icon'        => 'icon-calculator',
                'class'       => 'Oppin\POS\Models\Settings',
                'order'       => 500,
                'keywords'    => 'tax pos',
                'permissions' => ['oppin.pos.access_settings']
            ]
        ];
    }
}
