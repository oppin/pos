<?php
// TODO: Auth check middleware - users for middleware purposes will be terminals
Route::group([
    'prefix'     => 'api',
    'middleware' => 'Oppin\POS\Middleware\TerminalAuth',
], function() {
    Route::group(['prefix' => 'v1'], function() {
        Route::group(['prefix' => 'reports'], function() {
            Route::get('totals/{date?}', 'Oppin\POS\API\V1\Reports@totals');
        });
        Route::group(['prefix' => 'transactions'], function() {
            Route::post('/', 'Oppin\POS\API\V1\Transactions@post');
            Route::get('latest', 'Oppin\POS\API\V1\Transactions@latest');
        });
        Route::group(['prefix' => 'terminal'], function() {
            Route::get('update', 'Oppin\POS\API\V1\Terminal@update');
        });
    });
});
