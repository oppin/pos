<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_products');
        Schema::create('oppin_pos_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable()->index();
            $table->integer('status_id')->unsigned()->index();
            $table->string('name')->index();
            $table->string('short_name', 20)->nullable()->index();
            $table->string('sku', 30)->nullable()->index();
            $table->float('cost_price')->unsigned()->nullable();
            $table->string('cost_price_currency', 3)->default('GBP')->index();
            $table->float('cost_price_per')->unsigned()->nullable()->index();
            $table->string('price_per_unit', 10)->nullable();
            $table->json('meta')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_products');
    }
}
