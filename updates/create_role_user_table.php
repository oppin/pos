<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRoleUserTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('role_user');
        Schema::create('role_user', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('role_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->primary(['role_id', 'user_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('role_user');
    }
}
