<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEndOfDaysTable extends Migration
{
    public function up()
    {
        Schema::create('oppin_pos_end_of_days', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('transaction_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_end_of_days');
    }
}
