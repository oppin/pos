<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTransactionLinesTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_transaction_lines');
        Schema::create('oppin_pos_transaction_lines', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('transaction_id')->unsigned()->index();
            $table->integer('object_id')->unsigned()->nullable();
            $table->string('object_type')->nullable();
            $table->string('name')->nullable();
            $table->string('short_name', 20)->nullable();
            $table->string('tax_rate', 30)->nullable();
            $table->float('qty')->nullable();
            $table->float('single_amount')->nullable();
            $table->float('line_amount')->nullable();
            $table->float('line_tax')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_transaction_lines');
    }
}
