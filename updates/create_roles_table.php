<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRolesTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_roles');
        Schema::create('oppin_pos_roles', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 50);
            $table->json('permissions')->nullable();
            $table->boolean('is_system');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_roles');
    }
}
