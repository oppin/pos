<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductStatusesTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_product_statuses');
        Schema::create('oppin_pos_product_statuses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 50);
            $table->boolean('is_active')->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_product_statuses');
    }
}
