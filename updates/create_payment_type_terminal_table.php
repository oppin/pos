<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePaymentTypeTerminalTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('payment_type_terminal');
        Schema::create('payment_type_terminal', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('payment_type_id')->unsigned()->index();
            $table->integer('terminal_id')->unsigned()->index();
            $table->primary(['payment_type_id', 'terminal_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('payment_type_terminal');
    }
}
