<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBarcodesTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_barcodes');
        Schema::create('oppin_pos_barcodes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('object_id')->unsigned()->nullable();
            $table->string('object_type')->nullable();
            $table->string('barcode', 30)->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_barcodes');
    }
}
