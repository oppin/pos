<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTransactionsTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_transactions');
        Schema::create('oppin_pos_transactions', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('client_id')->nullable()->unsigned()->index();
            $table->integer('parent_id')->nullable()->unsigned()->index();
            $table->integer('terminal_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->string('currency', 3)->default('GBP')->index();
            $table->enum('status', [
                'open',
                'complete',
                'suspended',
                'void'
            ])->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_transactions');
    }
}
