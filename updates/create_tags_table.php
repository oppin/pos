<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTagsTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_tags');
        Schema::create('oppin_pos_tags', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable()->index();
            $table->integer('nest_left')->unsigned()->nullable()->index();
            $table->integer('nest_right')->unsigned()->nullable()->index();
            $table->integer('nest_depth')->unsigned()->nullable()->index();
            $table->string('name', 50)->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_tags');
    }
}
