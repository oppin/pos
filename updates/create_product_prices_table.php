<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductPricesTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_product_prices');
        Schema::create('oppin_pos_product_prices', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->unsigned()->nullable()->index();
            $table->float('min_qty')->default(1)->unsigned()->index();
            $table->string('currency', 3)->default('GBP')->index();
            $table->float('sale_price')->unsigned();
            $table->string('tax_rate', 30);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_product_prices');
    }
}
