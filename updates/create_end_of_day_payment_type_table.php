<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEndOfDayPaymentTypeTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('end_of_day_payment_type');
        Schema::create('end_of_day_payment_type', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('end_of_day_id')->unsigned()->index();
            $table->integer('payment_type_id')->unsigned()->index();
            $table->float('start')->unsigned()->default(0);
            $table->float('expected')->unsigned()->default(0);
            $table->float('actual')->unsigned()->default(0);
            $table->float('carry')->unsigned()->default(0);
            $table->primary(['end_of_day_id', 'payment_type_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('end_of_day_payment_type');
    }
}
