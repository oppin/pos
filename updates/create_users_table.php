<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_users');
        Schema::create('oppin_pos_users', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('first_name', 20)->index();
            $table->string('last_name', 30)->index();
            $table->string('login', 5)->index();
            $table->boolean('is_active')->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_users');
    }
}
