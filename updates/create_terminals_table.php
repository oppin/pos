<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTerminalsTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_terminals');
        Schema::create('oppin_pos_terminals', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('api_key', 30)->index();
            $table->integer('location_id')->unsigned()->index();
            $table->integer('transaction_id')->unsigned()->nullable()->index();
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->string('name', 50)->index();
            $table->string('currency', 3)->default('GBP');
            $table->boolean('is_active')->index();
            $table->enum('status', [
                'unknown',
                'closed',
                'signed_off',
                'single_sign_on',
                'signed_on',
            ])->default('unknown');
            $table->timestamp('last_seen')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_terminals');
    }
}
