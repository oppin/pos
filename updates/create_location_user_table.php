<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLocationUserTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('location_user');
        Schema::create('location_user', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('location_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->primary(['location_id', 'user_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('location_user');
    }
}
