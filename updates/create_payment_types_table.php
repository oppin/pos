<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePaymentTypesTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_pos_payment_types');
        Schema::create('oppin_pos_payment_types', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 30);
            $table->string('short_name', 20);
            $table->string('currency', 3)->default('GBP')->index();
            $table->boolean('is_active');
            $table->boolean('cash');
            $table->boolean('show_other');
            $table->json('amounts')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_pos_payment_types');
    }
}
