<?php namespace Oppin\POS\Updates;

use Seeder;
use Oppin\POS\Models\Location;
use Oppin\POS\Models\Terminal;
use Oppin\POS\Models\User;
use Oppin\POS\Models\Role;
use Oppin\POS\Models\Product;
use Oppin\POS\Models\ProductStatus;
use Oppin\POS\Models\Tag;
use Oppin\POS\Models\PaymentType;

class SeedLocationsAndTerminals extends Seeder
{
    public function run()
    {
        $location = new Location([
            'name'        => 'My Store',
            'buttons'     => [[
                '_group'      => 'product',
                'product_id'  => 1
            ]],
        ]);
        $location->forceSave();

        $terminal = new Terminal([
            'location_id' => $location->id,
            'name'        => 'Front Till',
            'currency'    => 'GBP',
            'is_active'   => true,
            'status'      => 'unknown',
        ]);
        $terminal->forceSave();

        $assistantRole = Role::create([
            'name'        => 'Sales Assistant',
            'permissions' => null,
            'is_system'   => true,
        ]);

        $supervisorRole = Role::create([
            'name'        => 'Supervisor',
            'permissions' => null,
            'is_system'   => true,
        ]);

        $managerRole = Role::create([
            'name'        => 'Manager',
            'permissions' => null,
            'is_system'   => true,
        ]);

        $user = User::create([
            'first_name'  => 'John',
            'last_name'   => 'Watson',
            'is_active'   => true,
        ]);
        $user->locations()->attach($location->id);
        $user->roles()->attach($assistantRole->id);

        $user = User::create([
            'first_name'  => 'Sherlock',
            'last_name'   => 'Holmes',
            'is_active'   => true,
        ]);
        $user->locations()->attach($location->id);
        $user->roles()->attach([
            $assistantRole->id,
            $supervisorRole->id,
        ]);

        $user = User::create([
            'first_name'  => 'Tobias',
            'last_name'   => 'Gregson',
            'is_active'   => true,
        ]);
        $user->locations()->attach($location->id);
        $user->roles()->attach([
            $assistantRole->id,
            $supervisorRole->id,
            $managerRole->id,
        ]);

        $productActive = ProductStatus::create([
            'name'      => 'Active',
            'is_active' => true,
        ]);

        $product = new Product([
            'name'           => 'Hound of the Baskervilles',
            'short_name'     => 'Hound Baskervilles',
            'type'           => 'simple',
            'status_id'      => $productActive->id,
            'pricing_type'   => 'simple',
            'price_per_unit' => 'ea',
            'sale_price'     => '99.99',
            'tax_rate'       => 'gb_vat_exempt',
            'sku'            => '001',
        ]);
        $product->forceSave();
        $product->tags()->create(
            ['name' => 'Dogs'],
            ['primary' => true]
        );
        $product->tags()->create(['name' => 'Animals']);

        $paymentType = new PaymentType([
            'name'       => 'Cash',
            'short_name' => 'Cash',
            'currency'   => 'GBP',
            'is_active'  => true,
            'cash'       => true,
            'show_other' => true,
            'amounts'    => [
                50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01
            ]
        ]);
        $paymentType->forceSave();
        $paymentType = new PaymentType([
            'name'       => 'Card',
            'short_name' => 'Card',
            'currency'   => 'GBP',
            'is_active'  => true,
            'cash'       => false,
            'show_other' => true,
        ]);
        $paymentType->forceSave();
        $paymentType = new PaymentType([
            'name'       => 'Voucher',
            'short_name' => 'Voucher',
            'currency'   => 'GBP',
            'is_active'  => true,
            'cash'       => false,
            'show_other' => true,
        ]);
        $paymentType->forceSave();

        $terminal->payment_types()->sync([1, 2, 3]);
    }
}
