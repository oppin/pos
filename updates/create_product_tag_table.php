<?php namespace Oppin\POS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductTagTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('product_tag');
        Schema::create('product_tag', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('product_id')->unsigned()->index();
            $table->integer('tag_id')->unsigned()->index();
            $table->boolean('primary')->default(0)->index();
            $table->primary(['product_id', 'tag_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_tag');
    }
}
