<?php

return [
    'currency_code' => 'That currency code isn\'t recognised.',
    'tax_rate'      => 'That tax rate isn\'t available.',
    'tax_type'      => 'That tax type isn\'t recognised.',
];
