<?php namespace Oppin\POS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Oppin\POS\Models\User;

/**
 * Users Back-end Controller
 */
class Users extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Oppin.POS', 'users', 'users');
    }

    public function onRegenerateLogin($recordId)
    {
        $user = $this->formFindModelObject($recordId);
        do {
            $user->login = str_pad(rand(0, 99999), 5, '0', STR_PAD_LEFT);
        } while (User::where('login', $user->login)->count());
        $user->save();
        return ['login' => $user->login];
    }
}
