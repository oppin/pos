<?php namespace Oppin\POS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Oppin\POS\Models\Location;

/**
 * Locations Back-end Controller
 */
class Locations extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Oppin.POS', 'locations', 'locations');
    }
}
