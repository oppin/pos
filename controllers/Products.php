<?php namespace Oppin\POS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Oppin\POS\Models\Product;

/**
 * Products Back-end Controller
 */
class Products extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Oppin.POS', 'pos', 'products');
    }

    //
    // List
    //

    public function listExtendQuery($query)
    {
        $query->with([
            'status',
            'tags' => function ($query) {
                $query->orderBy('primary', 'desc')
                    ->orderBy('name', 'asc');
            }
        ])->whereNull('parent_id');
    }

    //
    // Form
    //

    public function formExtendQuery($query)
    {
        $query->whereNull('parent_id');
    }
}
