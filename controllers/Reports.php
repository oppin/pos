<?php namespace Oppin\POS\Controllers;

use Backend;
use BackendMenu;
use DB;
use Redirect;
use Backend\Classes\Controller;
use October\Rain\Auth\AuthException;
use Oppin\POS\Models\EndOfDay;
use Oppin\POS\Models\Location;
use Oppin\POS\Models\Payment;
use Oppin\POS\Models\Product;
use Oppin\POS\Models\Settings;
use Oppin\POS\Models\Transaction;
use Oppin\POS\Models\TransactionLine;
use CommerceGuys\Tax\Repository\TaxTypeRepository;
use CommerceGuys\Tax\Model\TaxType;
use Carbon\Carbon;

/**
 * Reports Back-end Controller
 */
class Reports extends Controller
{
    public $implement = [
        'Backend.Behaviors.ListController'
    ];

    public $listConfig = [
        'eods'         => 'config_eods_list.yaml',
        'products'     => 'config_products_list.yaml',
        'transactions' => 'config_transactions_list.yaml',
    ];

    public $requiredPermissions = ['oppin.pos.reports.*'];

    /**
     * @var array Collection of functions to apply to each total query.
     */
    protected $totalsFilterCallbacks = [];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Oppin.POS', 'reports', 'eod');
    }

    public function index(Type $var = null)
    {
        if ($this->user->hasAccess('oppin.pos.reports.eod')) {
            return Redirect::to(Backend::url('oppin/pos/reports/eods'));
        } elseif ($this->user->hasAccess('oppin.pos.reports.products')) {
            return Redirect::to(Backend::url('oppin/pos/reports/products'));
        } elseif ($this->user->hasAccess('oppin.pos.reports.transactions')) {
            return Redirect::to(Backend::url('oppin/pos/reports/transactions'));
        }
    }

    public function eods()
    {
        if (!$this->user->hasAccess('oppin.pos.reports.eod')) {
            throw new AuthException;
        }

        BackendMenu::setContext('Oppin.POS', 'reports', 'eod');

        $this->pageTitle = 'EOD Reports';
        $this->bodyClass = 'compact-container';
        $this->makeLists();
    }

    public function eod($recordId)
    {
        if (!$this->user->hasAccess('oppin.pos.reports.eod')) {
            throw new AuthException;
        }

        BackendMenu::setContext('Oppin.POS', 'reports', 'eod');

        $this->pageTitle = 'Details';
        $this->vars['eod'] = $eod = EndOfDay::with('transaction.lines')
            ->with([
                'transaction.terminal' => function($query) {
                    $query->withTrashed()->with([
                        'location' => function($query) {
                            $query->withTrashed();
                        }
                    ]);
                },
                'transaction.user' => function($query) {
                    $query->withTrashed();
                },
                'payment_types' => function($query) {
                    $query->withTrashed();
                }
            ])
            ->findOrFail($recordId);
        $this->vars['lines'] = $eod->transaction->lines;
    }

    public function products()
    {
        if (!$this->user->hasAccess('oppin.pos.reports.products')) {
            throw new AuthException;
        }

        BackendMenu::setContext('Oppin.POS', 'reports', 'products');

        $this->pageTitle = 'Product Sales Totals';
        $this->bodyClass = 'slim-container';
        $this->makeLists();
    }

    public function totals()
    {
        if (!$this->user->hasAccess('oppin.pos.reports.totals')) {
            throw new AuthException;
        }

        BackendMenu::setContext('Oppin.POS', 'reports', 'totals');

        $this->pageTitle = 'Daily Totals';
        $this->bodyClass = 'compact-container';

        $filterConfig = $this->makeConfig('config_totals_filter.yaml');
        $filterConfig->alias = 'Filter';
        $filterWidget = $this->makeWidget('Backend\Widgets\Filter', $filterConfig);
        $filterWidget->addScopes([
            'created_at' => [
                'label' => 'Date',
                'type'  => 'daterange',
                // 'default' => [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()],
                'default' => [Carbon::parse('2019-09-19 00:00:00'), Carbon::parse('2019-08-31 23:59:59')],
                'conditions' => 'oppin_pos_transaction_lines.created_at >= ":after" AND oppin_pos_transaction_lines.created_at <= ":before"',
            ]
        ]);
        $filterWidget->bindToController();

        /*
        * Filter the list when the scopes are changed
        */
        $filterWidget->bindEvent('filter.update', function () use ($filterWidget) {
            $this->prepareTotals();

            return [
                '#tbody-product-sales' => $this->makePartial('transactions_rows'),
                '#tbody-tax'           => $this->makePartial('tax_rows'),
                '#tbody-payments'      => $this->makePartial('payments_rows')
            ];
        });

        // Apply predefined filter values
        $this->addTotalsFilter([$filterWidget, 'applyAllScopesToQuery']);

        $this->vars['filter'] = $filterWidget;

        $this->prepareTotals();
    }

    public function prepareTotals()
    {
        $repo = new TaxTypeRepository();
        $types = $repo->getAll();
        $taxRates = [];
        foreach ($types as $type) {
            $taxRates[$type->getId() . '_exempt'] = $type->getName() . ' Exempt (0%)';
            $typeRates = $type->getRates();
            foreach ($typeRates as $rate) {
                foreach ($rate->getAmounts() as $amount) {
                    if (is_null($amount->getEndDate())) {
                        $taxRates[$rate->getId()] = $type->getName() . ' ' . $rate->getName() . ' (Currently ' . $amount->getAmount() * 100 . '%)';
                    }
                }
            }
        }

        $transactionsQuery = TransactionLine::whereHas('transaction', function($query) {
                $query->where('status', 'complete');
            })
            ->where('object_type', 'Oppin\POS\Models\Product')
            ->addSelect(DB::raw(
                'COUNT(DISTINCT transaction_id) AS transaction_count, ' .
                'COUNT(DISTINCT CASE WHEN line_amount > 0 THEN transaction_id ELSE NULL END) AS income_count, ' .
                'COUNT(DISTINCT CASE WHEN line_amount < 0 THEN transaction_id ELSE NULL END) AS refund_count, ' .
                'SUM(CASE WHEN line_amount > 0 THEN line_amount ELSE 0 END) AS income_total_amount, ' .
                'SUM(CASE WHEN line_tax > 0 THEN line_tax ELSE 0 END) AS income_total_tax, ' .
                'SUM(CASE WHEN line_amount < 0 THEN line_amount * -1 ELSE 0 END) AS refund_total_amount, ' .
                'SUM(CASE WHEN line_tax < 0 THEN line_tax * -1 ELSE 0 END) AS refund_total_tax, ' .
                'SUM(line_amount) AS total_amount, ' .
                'SUM(line_tax) AS total_tax'
            ));

        $taxQuery = TransactionLine::whereHas('transaction', function($query) {
                $query->where('status', 'complete');
            })
            ->where('object_type', 'Oppin\POS\Models\Product')
            ->select('tax_rate', DB::raw(
                'COUNT(DISTINCT transaction_id) AS transaction_count, ' .
                'SUM(line_amount) AS total_amount, ' .
                'SUM(line_tax) AS total_tax'
            ))
            ->groupBy('tax_rate');

        $paymentsQuery = Payment::whereHas('transaction_line', function($query) {
                $query->whereHas('transaction', function($query) {
                    $query->where('status', 'complete');
                });
            })
            ->join('oppin_pos_payment_types AS payment_types', 'oppin_pos_payments.payment_type_id', '=', 'payment_types.id')
            ->join('oppin_pos_transaction_lines', function($join) {
                $join->on('oppin_pos_payments.id', '=', 'oppin_pos_transaction_lines.object_id')
                    ->where('oppin_pos_transaction_lines.object_type', Payment::class)
                    ->where('oppin_pos_transaction_lines.created_at', '>=', '2019-01-01 00:00:00')
                    ->where('oppin_pos_transaction_lines.created_at', '<=', '2019-01-31 23:59:59');
            })
            ->locations([1])
            ->groupBy('payment_type_id')
            ->addSelect(DB::raw(
                'payment_types.name, ' .
                'payment_types.short_name, ' .
                'COUNT(DISTINCT oppin_pos_transaction_lines.transaction_id) AS transaction_count, ' .
                'SUM(CASE WHEN amount > 0 THEN amount ELSE 0 END) AS income_total_amount, ' .
                'SUM(CASE WHEN amount < 0 THEN amount * -1 ELSE 0 END) AS refund_total_amount, ' .
                'SUM(amount) AS total_amount'
            ));

        /*
         * Apply filters
         */
        foreach ($this->totalsFilterCallbacks as $callback) {
            $callback($transactionsQuery);
            $callback($taxQuery);
            // $callback($paymentsQuery);
        }

        $taxTypes = $taxQuery->get()->map(function($record) use ($taxRates) {
            if ($record->tax_rate) {
                $record->tax_rate_name = $taxRates[$record->tax_rate];
            } else {
                $record->tax_rate_name = 'None';
            }
            return $record;
        });

        // dd($transactionsQuery->toSql(), $transactionsQuery->getBindings(), $transactionsQuery->first(), $paymentsQuery->toSql(), $paymentsQuery->getBindings());

        $this->vars['transactions'] = $transactionsQuery->first();
        $this->vars['tax'] = $taxTypes;
        $this->vars['payments'] = $paymentsQuery->get();
    }

    public function addTotalsFilter(callable $filter)
    {
        $this->totalsFilterCallbacks[] = $filter;
    }

    public function transactions()
    {
        if (!$this->user->hasAccess('oppin.pos.reports.transactions')) {
            throw new AuthException;
        }

        BackendMenu::setContext('Oppin.POS', 'reports', 'transactions');

        $this->pageTitle = 'Transactions';
        $this->bodyClass = 'compact-container';
        $this->makeLists();
    }

    public function transaction($recordId)
    {
        if (!$this->user->hasAccess('oppin.pos.reports.transactions')) {
            throw new AuthException;
        }

        BackendMenu::setContext('Oppin.POS', 'reports', 'transactions');

        $this->pageTitle = 'Details';
        $this->vars['transaction'] = Transaction::with('lines')
            ->with([
                'terminal' => function($query) {
                    $query->withTrashed()->with([
                        'location' => function($query) {
                            $query->withTrashed();
                        }
                    ]);
                },
                'user' => function($query) {
                    $query->withTrashed();
                }
            ])
            ->findOrFail($recordId);
    }

    public function listExtendQuery($query, $definition)
    {
        if ($definition == 'eods') {
            $totals = DB::table('end_of_day_payment_type')
                ->select(
                    'end_of_day_id',
                    DB::raw('SUM(expected) AS expected'),
                    DB::raw('SUM(actual) AS actual'),
                    DB::raw('SUM(carry) AS carry')
                )
                ->groupBy('end_of_day_id');

            $query->with('transaction.terminal.location')
                ->addSelect('expected', 'actual', 'carry', DB::raw('(actual - expected) AS imbalance'), DB::raw('(actual - carry) AS banked'))
                ->join(
                    DB::raw('(' . $totals->toSql() . ') AS totals'),
                    'oppin_pos_end_of_days.id', '=', 'totals.end_of_day_id'
                )
                ->orderBy('id', 'desc');
        }

        if ($definition == 'products') {
            $query->select(
                    DB::raw('MAX(name) as name'),
                    DB::raw('SUM(CASE when qty > 0 THEN qty ELSE 0 END) as sold'),
                    DB::raw('SUM(CASE when qty > 0 THEN line_amount ELSE 0 END) as sold_total'),
                    DB::raw('ABS(SUM(CASE when qty < 0 THEN qty ELSE 0 END)) as refunded'),
                    DB::raw('ABS(SUM(CASE when qty < 0 THEN line_amount ELSE 0 END)) as refunded_total')
                )
                ->where('object_type', Product::class)
                ->whereHas('transaction', function($query) {
                    $query->where('status', 'complete');
                })
                ->groupBy('object_id');
        }
    }
}
