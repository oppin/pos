<?php namespace Oppin\POS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Oppin\POS\Models\Terminal;

/**
 * Terminals Back-end Controller
 */
class Terminals extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Oppin.POS', 'terminals', 'terminals');
    }

    public function onRegenerateApiKey($recordId)
    {
        $terminal = $this->formFindModelObject($recordId);
        do {
            $terminal->api_key = str_random(30);
        } while (Terminal::where('api_key', $terminal->api_key)->count());
        $terminal->save();
        return ['key' => $terminal->api_key];
    }
}
