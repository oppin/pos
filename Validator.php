<?php namespace Oppin\POS;

use CommerceGuys\Intl\Currency\CurrencyRepository;
use Illuminate\Validation\Validator as ValidatorBase;
use Oppin\POS\Models\Settings;
use CommerceGuys\Tax\Repository\TaxTypeRepository;

class Validator extends ValidatorBase
{
    /**
     * Check currency code is valid
     * @param string $attribute
     * @param string $value
     * @param array  $parameters
     * @return bool
     */
    public function validateCurrencyCode($attribute, $value, array $parameters)
    {
        $repo = new CurrencyRepository;
        $list = $repo->getList();
        return in_array($value, array_keys($list));
    }

    /**
     * Check tax type is valid
     * @param string $attribute
     * @param string $value
     * @param array  $parameters
     * @return bool
     */
    public function validateTaxType($attribute, $value, array $parameters)
    {
        $repo = new TaxTypeRepository;
        $list = $repo->getAll();
        return in_array($value, array_keys($list));
    }

    /**
     * Check tax rate exists
     * @param string $attribute
     * @param string $value
     * @param array  $parameters
     * @return bool
     */
    public function validateTaxRate($attribute, $value, array $parameters)
    {
        $taxRates = array_keys((new Settings)->getTaxRatesOptions());
        return true; //in_array($value, $taxRates);
    }
}
